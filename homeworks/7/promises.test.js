const { promiseAll, promiseAllSettled, chainPromises, promisify} = require('./promises');

describe('Promises', () => {
    const promises = [
        new Promise((resolve) => setTimeout(() => resolve(1), 3000)),
        new Promise((resolve) => setTimeout(() => resolve(2), 2000)),
        new Promise((resolve) => setTimeout(() => resolve(3), 1000)),
    ];

    describe('PromiseAll', () => {
       test('should return values of all resolved promises', () => {
           return promiseAll(promises).then(results => expect(results).toStrictEqual([1, 2, 3]));
       });

       test('should reject a promise with the first occurred error', () => {
           const promisesWithError = [
               new Promise((resolve) => setTimeout(() => resolve(1), 3000)),
               new Promise((resolve) => setTimeout(() => resolve(2), 2000)),
               new Promise((resolve, reject) => setTimeout(() => reject(new Error('Error occurred!')), 2000)),
               new Promise((resolve) => setTimeout(() => resolve(3), 1000)),
           ];

           return promiseAll(promisesWithError).catch(({ message }) => expect(message).toBe('Error occurred!'));
       });
    });

    describe('PromiseAllSettled', () => {
        test('should return values from all passed promises', () => {
            const res1 = [
                { status: 'fulfilled', value: 1 },
                { status: 'fulfilled', value: 2 },
                { status: 'fulfilled', value: 3 },
            ];

            return promiseAllSettled(promises).then(res => expect(res).toStrictEqual(res1));
        });

        test('should return values from all passed promises including errors', () => {
            const promisesWithError = [
                new Promise((resolve) => setTimeout(() => resolve(1), 3000)),
                new Promise((resolve) => setTimeout(() => resolve(2), 2000)),
                new Promise((resolve, reject) => setTimeout(() => reject(new Error('Error occurred!')), 2000)),
                new Promise((resolve) => setTimeout(() => resolve(3), 1000))
            ];

            const res2 = [
                { status: 'fulfilled', value: 1 },
                { status: 'fulfilled', value: 2 },
                { status: 'rejected', reason: 'Error occurred!' },
                { status: 'fulfilled', value: 3 },
            ];

            return promiseAllSettled(promisesWithError).then(res => expect(res).toStrictEqual(res2));
        });
    });

    describe('ChainPromises', () => {
        function asyncFunction1() {
            return new Promise(resolve => setTimeout(() => resolve('Result from asyncFunction1'), 2000));
        }

        function asyncFunction2(data) {
            return new Promise(resolve => setTimeout(() => resolve(data + ' - Result from asyncFunction2'), 1000));
        }

        function asyncFunction3(data) {
            return new Promise(resolve => setTimeout(() => resolve(data + ' - Result from asyncFunction3'), 1000));
        }

        test('should chain functions calls and return the united result', () => {
            const functionsArray = [asyncFunction1, asyncFunction2, asyncFunction3];
            const result = 'Result from asyncFunction1 - Result from asyncFunction2 - Result from asyncFunction3';

            return chainPromises(functionsArray).then(res => expect(res).toBe(result));
        });

        test('should chain functions calls and return the error if it occurred', () => {
            function asyncFunctionWithError() {
                return new Promise((resolve, reject) => setTimeout(() => reject(new Error('Error in function')), 1000));
            }
            const functionsArray = [asyncFunction1, asyncFunction2, asyncFunctionWithError, asyncFunction3];

            return chainPromises(functionsArray).catch(({ message }) => expect(message).toBe('Error in function'));
        });
    });

    describe('Promisify', () => {
        function callbackStyleFunction(value, callback) {
            setTimeout(() => {
                if (value > 0) {
                    callback(null, value * 2);
                } else {
                    callback('Invalid value', null);
                }
            }, 1000);
        }

        const promisedFunction = promisify(callbackStyleFunction);

        test('should transform callback-based function into promise-based one and return a value', () => {
            return promisedFunction(3).then(res => expect(res).toBe(6));
        });

        test('should transform callback-based function into promise-based one and return an error', () => {
            return promisedFunction(-10).catch(err => expect(err).toBe('Invalid value'));
        });
    });
});