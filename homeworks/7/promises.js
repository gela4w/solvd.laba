module.exports.promiseAll = (promises) => {
    return new Promise((resolve, reject) => {
        let _results = [];
        let _count = 0;

        promises.forEach((promise, i) => {
            promise.then(res => {
                _results[i] = res;
                _count++;

                if(_count === promises.length) {
                    resolve(_results);
                }
            }).catch(reject);
        });
    });
};

module.exports.promiseAllSettled = (promises) => {
    return new Promise((resolve) => {
        let _results = [];
        let _count = 0;

        promises.forEach((promise, i) => {
            promise
                .then(value => _results[i] = { status: 'fulfilled', value })
                .catch(({ message }) => _results[i] = { status: 'rejected', reason: message })
                .finally(() => {
                    _count++;
                    if(_count === promises.length) {
                        resolve(_results);
                    }
                });
        });
    });
};

module.exports.chainPromises = (functions) => {
    return new Promise((resolve, reject) => {
        let _result = functions[0]();

        for(let i = 1; i < functions.length; i++) {
            _result = _result.then(res => functions[i](res)).catch(reject);
        }

        _result.then(resolve);
    });
};

module.exports.promisify = (fn) => {
    return function (...args) {
        return new Promise((resolve, reject) => {
            const cb = (err, result) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            };

            args.push(cb);
            fn.apply(this, args);
        });
    };
};