const { addValues, stringifyValues, invertBoolean, convertToNumber, coerceToType } = require('./2-data-transformation-functions');

describe('Data Transformation Functions', () => {
    const errorMessage = 'Operation cannot be performed. Incorrect argument type.';

    describe('AddValues', () => {
        test('should add two numbers', () => {
            expect(addValues(1, 2)).toBe(3);
        });

        test('should concatenate two strings', () => {
            expect(addValues('1', '2')).toBe('12');
        });

        test('should throw an error if both arguments\' types are neither number nor string', () => {
           expect(() => addValues(1, '2')).toThrow(errorMessage);
           expect(() => addValues({}, 'abc')).toThrow(errorMessage);
        });
    });

    describe('StringifyValue', () => {
        describe('Primitives conversion to a string type', () => {
            test('should convert a number', () => {
                expect(stringifyValues(1)).toBe('1');
            });

            test('should convert a boolean', () => {
                expect(stringifyValues(true)).toBe('true');
            });

            test('should convert an undefined', () => {
                expect(stringifyValues(undefined)).toBe('undefined');
            });
        });

        describe('Non-primitives conversion to a string type', () => {
            test('should convert an object', () => {
                expect(stringifyValues({})).toBe('{}');
                expect(stringifyValues({ a: 'test', b: '1' })).toBe('{"a":"test","b":"1"}');
            });

            test('should convert an array', () => {
                expect(stringifyValues([])).toBe('[]');
                expect(stringifyValues([1, 2, 'a'])).toBe('[1,2,"a"]');
            });

            test('should convert a null', () => {
               expect(stringifyValues(null)).toBe('null');
            });
        });
    });

    describe('InvertBoolean', () => {
        test('should invert a true value', () => {
            expect(invertBoolean(true)).toBe(false);
        });

        test('should invert a false value', () => {
            expect(invertBoolean(false)).toBe(true);
        });

        test('should throw an error if provided argument is not of type boolean', () => {
            expect(() => invertBoolean(1)).toThrow(errorMessage);
        })
    });

    describe('ConvertToNumber', () => {
        describe('String conversions', () => {
            test('should convert a sting', () => {
                expect(convertToNumber('123')).toBe(123);
                expect(convertToNumber('123.45')).toBe(123.45);
                expect(convertToNumber('0abc')).toBe(0);
            });

            test('should throw an error if string conversion is not possible', () => {
                expect(() => convertToNumber('abc')).toThrow(errorMessage);
                expect(() => convertToNumber('abc123')).toThrow(errorMessage);
            })
        });
        describe('Other types conversions', () => {
            test('should convert a null value', () => {
                expect(convertToNumber(null)).toBe(0);
            });

            test('should correctly handle an array conversion', () => {
                expect(convertToNumber([])).toBe(0);
                expect(convertToNumber([1])).toBe(1);
                expect(() => convertToNumber([1, 2, 3])).toThrow(errorMessage);
            });

            test('should convert a boolean value', () => {
                expect(convertToNumber(true)).toBe(1);
                expect(convertToNumber(false)).toBe(0);
            });

            test('should throw an error if conversion is not possible', () => {
                expect(() => convertToNumber({})).toThrow(errorMessage);
                expect(() => convertToNumber({ a: 123 })).toThrow(errorMessage);
                expect(() => convertToNumber(undefined)).toThrow(errorMessage);
            });
        });

        test('should correctly handle provided argument of type number', () => {
            expect(convertToNumber(123)).toBe(123);
            expect(() => convertToNumber(NaN)).toThrow(errorMessage);
        });
    });

    describe('CoerceToType', () => {
        const Types = {
            Number: 'number',
            String: 'string',
            Boolean: 'boolean'
        };

        test('should convert a number', () => {
            expect(coerceToType(1, Types.String)).toBe('1');
            expect(coerceToType(NaN, Types.String)).toBe('NaN');
            expect(coerceToType(1, Types.Number)).toBe(1);
            expect(() => coerceToType(NaN, Types.Number)).toThrow(errorMessage);
            expect(coerceToType(1, Types.Boolean)).toBe(true);
            expect(coerceToType(0, Types.Boolean)).toBe(false);
            expect(coerceToType(NaN, Types.Boolean)).toBe(false);
        });

        test('should convert a boolean', () => {
            expect(coerceToType(false, Types.String)).toBe('false');
            expect(coerceToType(false, Types.Number)).toBe(0);
            expect(coerceToType(true, Types.Number)).toBe(1);
            expect(coerceToType(false, Types.Boolean)).toBe(false);
        });

        test('should convert a string', () => {
            expect(coerceToType('abc123', Types.String)).toBe('abc123');
            expect(coerceToType('123', Types.Number)).toBe(123);
            expect(() => coerceToType('abc123', Types.Number)).toThrow(errorMessage);
            expect(() => coerceToType('NaN', Types.Number)).toThrow(errorMessage);
            expect(coerceToType('123', Types.Boolean)).toBe(true);
            expect(coerceToType('', Types.Boolean)).toBe(false);
            expect(coerceToType(' ', Types.Boolean)).toBe(true);
        });

        test('should convert a null', () => {
            expect(coerceToType(null, Types.String)).toBe('null');
            expect(coerceToType(null, Types.Number)).toBe(0);
            expect(coerceToType(null, Types.Boolean)).toBe(false);
        });

        test('should convert an undefined', () => {
            expect(coerceToType(undefined, Types.String)).toBe('undefined');
            expect(() => coerceToType(undefined, Types.Number)).toThrow(errorMessage);
            expect(coerceToType(undefined, Types.Boolean)).toBe(false);
        });

        test('should convert an array', () => {
            expect(coerceToType([], Types.String)).toBe('[]');
            expect(coerceToType([1, 2, 3], Types.String)).toBe('[1,2,3]');
            expect(coerceToType([], Types.Number)).toBe(0);
            expect(coerceToType([1], Types.Number)).toBe(1);
            expect(() => coerceToType([1, 2], Types.Number)).toThrow(errorMessage);
            expect(coerceToType([], Types.Boolean)).toBe(true);
            expect(coerceToType([1, 2], Types.Boolean)).toBe(true);
        });

        test('should convert an object', () => {
            expect(coerceToType({}, Types.String)).toBe('{}');
            expect(() => coerceToType({}, Types.Number)).toThrow(errorMessage);
            expect(coerceToType({}, Types.Boolean)).toBe(true);
            expect(coerceToType( { a: 'abc' }, Types.Boolean)).toBe(true);
        });

        test('should throw an error if provided an incorrect type to convert', () => {
            expect(() => coerceToType(1, 'abracadabra')).toThrow(errorMessage);
        });
    });
});