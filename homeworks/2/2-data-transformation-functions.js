const DataTypes = {
    Number: 'number',
    String: 'string',
    Boolean: 'boolean',
    Object: 'object'
};

class DataTransformationFunctions {
    static errorMessage = 'Operation cannot be performed. Incorrect argument type.';

    static addValues(arg1, arg2) {
        if (typeof arg1 === DataTypes.Number && typeof arg2 === DataTypes.Number ||
            typeof arg1 === DataTypes.String && typeof arg2 === DataTypes.String) {
            return arg1 + arg2;
        }

        throw new Error(DataTransformationFunctions.errorMessage);
    }

    static stringifyValues(arg) {
        if(typeof arg === DataTypes.String) {
            return arg;
        }

        return typeof arg === DataTypes.Object ? JSON.stringify(arg) : String(arg);
    }

    static invertBoolean(arg) {
        if(typeof arg !== DataTypes.Boolean) {
            throw new Error(DataTransformationFunctions.errorMessage);
        }

        return !arg;
    }

    static convertToNumber(arg) {
        if(typeof arg === DataTypes.Number) {
            if(Number.isNaN(arg)) {
                throw new Error(DataTransformationFunctions.errorMessage);
            }

            return arg;
        } else if(typeof arg === DataTypes.String) {
            const convertedValue = parseFloat(arg);

            if(Number.isNaN(convertedValue)) {
                throw new Error(DataTransformationFunctions.errorMessage);
            }

            return convertedValue;
        } else {
            const convertedValue = Number(arg);

            if(Number.isNaN(convertedValue)) {
                throw new Error(DataTransformationFunctions.errorMessage);
            }

            return convertedValue;
        }
    }

    static coerceToType(arg, type) {
        switch (type) {
            case DataTypes.Number:
                return DataTransformationFunctions.convertToNumber(arg);
            case DataTypes.String:
                return DataTransformationFunctions.stringifyValues(arg);
            case DataTypes.Boolean:
                return typeof arg === DataTypes.Boolean ? arg : Boolean(arg);
            default:
                throw new Error(DataTransformationFunctions.errorMessage);
        }
    }
}

module.exports = DataTransformationFunctions;