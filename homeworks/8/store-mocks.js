const { Book } = require('./classes');

module.exports.books = [
    new Book('Philosopher\'s Stone', 'J. K. Rowling', '123', 10.50, 100),
    new Book('Chamber of Secrets', 'J. K. Rowling', '456', 11, 15),
    new Book('Prisoner of Azkaban', 'J. K. Rowling', '789', 11.30, 0),
    new Book('Goblet of Fire', 'J. K. Rowling', '147', 10, 25),
    new Book('Order of the Phoenix', 'J. K. Rowling', '258', 12.70, 0),
    new Book('Half-Blood Prince', 'J. K. Rowling', '369', 14.85, 47),
    new Book('Deathly Hallows', 'J. K. Rowling', '421', 18, 5),
];
