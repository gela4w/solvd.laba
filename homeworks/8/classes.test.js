const { books} = require('./store-mocks');
const { User, Student, Senior} = require('./classes');

function getUsers() {
    return {
        john: new User('John Doe', 'john@doe.com'),
        student: new Student('Piter Parker', 'spider-man@test.com'),
        senior: new Senior('Harry Potter', 'harry@test.com'),
    }
}

describe('Classes Interaction', () => {
    describe('AddBook', () => {
        const { john, student, senior } = getUsers();

        test('should add different available books to the cart', () => {
            const book1 = books[0];
            const availability1 = book1.availability;
            const book2 = books[1];
            const availability2 = book2.availability;

            john.cart.add(book1);
            john.cart.add(book2);

            expect(john.cart.items.length).toBe(2);
            expect(john.cart.items[0]).toStrictEqual({ book: book1, amount: 1 });
            expect(john.cart.items[1]).toStrictEqual({ book: book2, amount: 1 });

            expect(books[0].availability).toBe(availability1 - 1);
            expect(books[1].availability).toBe(availability2 - 1);
        });

        test('should add several items of an available book to the cart', () => {
            const book = books[3];
            const availability = book.availability;

            student.cart.add(book);
            student.cart.add(book);
            student.cart.add(book);

            expect(student.cart.items.length).toBe(1);
            expect(student.cart.items[0]).toStrictEqual({ book, amount: 3 });
            expect(books[3].availability).toBe(availability - 3);
        });

        test('should throw an error if user tries to add an unavailable book', () => {
            const book = books[2];

            expect(book.availability).toBe(0);

            expect(() => senior.cart.add(book)).toThrow('This book is not available at the moment.');
        });
    });

    describe('RemoveBook', () => {
        const { john, student, senior } = getUsers();

        test('should remove books from the cart', () => {
            const book1 = books[0];
            const availability1 = book1.availability;
            const book2 = books[1];
            const availability2 = book2.availability;

            john.cart.add(book1);
            john.cart.add(book2);

            expect(books[0].availability).toBe(availability1 - 1);
            expect(books[1].availability).toBe(availability2 - 1);

            john.cart.remove(book2);

            expect(john.cart.items.length).toBe(1);
            expect(books[1].availability).toBe(availability2);

            john.cart.remove(book1);

            expect(john.cart.items.length).toBe(0);
            expect(books[0].availability).toBe(availability1);
        });

        test('should remove one of the several same books from the cart', () => {
            const book = books[5];
            const availability = book.availability;

            student.cart.add(book);
            student.cart.add(book);
            student.cart.add(book);

            expect(student.cart.items[0].amount).toBe(3);
            expect(books[5].availability).toBe(availability - 3);

            student.cart.remove(book);
            student.cart.remove(book);

            expect(student.cart.items[0].amount).toBe(1);
            expect(books[5].availability).toBe(availability - 1);
        });

        test('should throw an error if user tries to remove a book that is not in the cart', () => {
            senior.cart.add(books[1]);

            expect(() => senior.cart.remove(books[0])).toThrow('There is no such book in the cart.')
        });
    });

    describe('MakeOrder', () => {
        const { john, student, senior } = getUsers();
        const [book1, book2, book3] = [books[0], books[1],books[6]];

        test('should make an order', () => {
            john.cart.add(book1);
            john.cart.add(book1);
            john.cart.add(book2);
            john.cart.add(book3);

            const order = john.makeOrder();
            const res = 'Order for: John Doe; includes: "Philosopher\'s Stone" by J. K. Rowling: 2 item(s),"Chamber of Secrets" by J. K. Rowling: 1 item(s),"Deathly Hallows" by J. K. Rowling: 1 item(s) with total price $50.00.';

            expect(order.toString()).toBe(res);
        });

        test('should correctly apply a discount to the order', () => {
            student.cart.add(book1);
            student.cart.add(book2);
            student.cart.add(book3);

            const studentOrder = student.makeOrder();

            senior.cart.add(book1);
            senior.cart.add(book2);
            senior.cart.add(book3);

            const seniorOrder = senior.makeOrder();

            expect(studentOrder.totalPrice).toBe('35.55');
            expect(seniorOrder.totalPrice).toBe('33.57');
        });
    });
});


