/** Class representing a book. */
class Book {
    /**
     * Create a book.
     * @param {string} title - The title of the book.
     * @param {string} author - The author of the book.
     * @param {string} ISBN - The ISBN of the book.
     * @param {number} price - The price of the book.
     * @param {number} availability - Amount of available copies of the book.
     */
    constructor(title, author, ISBN, price, availability) {
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
        this.price = price;
        this.availability = availability;
    }
}

/** Class representing a user. */
class User {
    /**
     * @property {Cart} - User's cart.
     */
    cart = new Cart();

    /**
     * Create a user.
     * @param {string} name - The name of the user.
     * @param {string} email - The email of the user.
     */
    constructor(name, email) {
        this.name = name;
        this.email = email;
    }

    /**
     * Get the discount the user has.
     * @return {number} The discount to be applied to the order total price.
     */
    get discount() {
        return 0;
    }

    /**
     * Make an order.
     * @return {Order} The order made by the user.
     */
    makeOrder() {
        const order = new Order(this, this.cart);
        this.cart = new Cart();

        return order;
    }
}

/**
 * Class representing a student user.
 * @extends User
 */
class Student extends User {
    /**
     * Create a student user.
     * @param {string} name - The name of the user.
     * @param {string} email - The email of the user.
     */
    constructor(name, email) {
        super(name, email);
    }

    /**
     * Get the discount the student user has.
     * @override
     * @return {number} The discount to be applied to the order total price.
     */
    get discount() {
        return 0.1;
    }
}

/**
 * Class representing a senior user.
 * @extends User
 */
class Senior extends User {
    /**
     * Create a user.
     * @param {string} name - The name of the user.
     * @param {string} email - The email of the user.
     */
    constructor(name, email) {
        super(name, email);
    }

    /**
     * Get the discount the senior user has.
     * @override
     * @return {number} The discount to be applied to the order total price.
     */
    get discount() {
        return 0.15;
    }
}

/** Class representing a cart. */
class Cart {
    /**
     * @property {Array} - Items in the user's cart.
     * @private
     */
    _items = [];

    /**
     * Get the items in the cart.
     * @return {Array} Books and their amount in the user's cart.
     */
    get items() {
        return this._items;
    }

    /**
     * Add a book to the cart.
     * @param {Book} book - The book to add to the cart.
     * @throws {Error} Throws an error if the book is not available.
     */
    add(book) {
        if(book.availability === 0) {
            throw new Error('This book is not available at the moment.');
        }

        let addedBook = this.items.find(item => item.book.ISBN === book.ISBN);

        if(addedBook) {
            addedBook.amount += 1;
        } else {
            this.items.push({ book, amount: 1 });
        }

        book.availability--;
    }

    /**
     * Remove a book from the cart.
     * @param {Book} book - The book to remove from the cart.
     * @throws {Error} Throws an error if the book is not in the cart.
     */
    remove(book) {
        const bookIndex = this.items.findIndex(item => item.book.ISBN === book.ISBN);

        if(bookIndex === -1) {
            throw new Error('There is no such book in the cart.');
        }

        if(this.items[bookIndex].amount > 1) {
            this.items[bookIndex].amount--;
        } else {
            this.items.splice(bookIndex, 1);
        }

        book.availability++;
    }

    /**
     * Get the total price of the items in the cart.
     * @return {string} The total price of the items in the cart.
     */
    getTotalPrice() {
        return this.items
            .map(({ book, amount }) => book.price * amount)
            .reduce((acc, curr) => acc + curr, 0).toFixed(2);
    }
}

/** Class representing an order. */
class Order {
    /**
     * Create an order.
     * @param {User} user - The user who made the order.
     * @param {Cart} cart - The cart containing the items in the order.
     */
    constructor(user, cart) {
        this.user = user;
        this.cart = cart;
    }

    /**
     * Get the books in the order.
     * @return {Array} Title, author and amount of books in the order.
     */
    get books() {
        return this.cart.items.map(({ book, amount }) => `"${book.title}" by ${book.author}: ${amount} item(s)`);
    }

    /**
     * Get the total price of the order.
     * @return {string} The total price of the order with the applied discount.
     */
    get totalPrice() {
        return (this.cart.getTotalPrice() * (1 - this.user.discount)).toFixed(2);
    }

    /**
     * Return the order's information as a string.
     * @override
     * @return {string} The order's information.
     */
    toString() {
        return `Order for: ${this.user.name}; includes: ${this.books} with total price $${this.totalPrice}.`
    }
}

module.exports = { Book, User, Student, Senior, Cart, Order };