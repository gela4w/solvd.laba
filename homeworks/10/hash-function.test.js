const hashFunc = require('./hash-function');

describe('Hash Function', () => {
    test('should hash the passed string in an idempotent way', () => {
        const str1 = 'Peter Parker';
        const hash1 = hashFunc(str1, 10);

        expect(hashFunc(str1, 10)).toBe(hash1);
        expect(hashFunc(str1, 10)).toBe(hash1);
        expect(hashFunc(str1, 10)).toBe(hash1);

        const str2 = 'Joe Doe Junior';
        const hash2 = hashFunc(str2, 53);

        expect(hashFunc(str2, 53)).toBe(hash2);
        expect(hashFunc(str2, 53)).toBe(hash2);
        expect(hashFunc(str2, 53)).toBe(hash2);
    });

    test('should distribute values uniformly', () => {
        const arr = [...Array(100).keys()];
        const size = 70;
        const hashes = arr.map(item => hashFunc(item.toString(), size));

        for(let i = 0; i < size; i++) {
            expect(hashes.includes(i)).toBeTruthy();
        }
    });
});