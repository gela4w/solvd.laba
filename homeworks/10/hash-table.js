const hashFn = require('./hash-function');

/** Class representing a node in the hash table. */
class Node {
    /**
     * Create a node.
     * @param {string} key - The key of the node.
     * @param {*} value - The value of the node.
     */
    constructor(key, value) {
        this.key = key;
        this.value = value;
        this.next = null;
    }
}

/**  Class representing a hash table.
 * Average time complexity of all operations is O(1),
 * in cases when collisions occur, complexity increases up to O(n);
 * to limit effect of the collisions re-balancing on fill factor >= 70% is performed.
 * Even though re-balancing time complexity is O(n),
 * in the worst scenario, it will be executed only log(n) times during the hash table life cycle.
 * */
class HashTable {
    /**
     * @property {Node[]} - Table containing key-value pairs.
     * @private
     */
    #table;

    /**
     * Create a hash table.
     * @param {number} [size=53] - The initial size of the hash table.
     */
    constructor(size = 53) {
        this.itemsCount = 0;
        this.#table = new Array(size);
    }

    /**
     * Get the table's size.
     * @return {number} The current size of the hash table.
     * Time complexity: O(1)
     */
    get size() {
        return this.#table.length;
    }

    /**
     * Check if the hash table needs resizing.
     * @return {boolean} True if resizing is required, false otherwise.
     * @private
     * Time complexity: O(1)
     */
    #isResizeRequired() {
        return this.itemsCount / this.size >= 0.7;
    }

    /**
     * Insert a key-value pair into the specified table.
     * @param {string} key - The key to insert.
     * @param {*} value - The value to insert.
     * @param {Node[]} table - The table to insert into.
     * @private
     * Time complexity: O(1)
     */
    #insert(key, value, table) {
        const index = hashFn(key, table.length);

        if(!table[index]) {
            table[index] = new Node(key, value);
        } else {
            let curr = table[index];

            while(curr.next) {
                if(curr.key === key) {
                    curr.value = value;

                    return;
                }
                curr = curr.next;
            }

            if(curr.key === key) {
                curr.value = value;
            } else {
                curr.next = new Node(key, value);
            }
        }
    }

    /**
     * Re-balance the hash table by increasing its size and reinserting all items.
     * @private
     * Time complexity: O(n)
     */
    #rebalance() {
        const newTable = new Array(this.size * 2);

        this.#table.filter(item => item).forEach(item => {
            let curr = item;

            while(curr) {
                this.#insert(curr.key, curr.value, newTable);
                curr = curr.next;
            }
        });

        this.#table = newTable;
    }

    /**
     * Add or update a key-value pair in the hash table.
     * @param {string} key - The key to set.
     * @param {*} value - The value to set.
     * Time complexity: O(1)
     */
    set(key, value) {
        if(this.#isResizeRequired()) {
            this.#rebalance();
        }

        this.#insert(key, value, this.#table);
        this.itemsCount++;
    }

    /**
     * Retrieve the value associated with a key.
     * @param {string} key - The key to retrieve.
     * @return {*} The value associated with the key.
     * @throws {Error} If the key does not exist in the table.
     * Time complexity: O(1)
     */
    get(key) {
        const index = hashFn(key, this.size);

        if(!this.#table[index]) {
            throw new Error('Such element does not exist in the table.');
        }

        let curr = this.#table[index];

        while(curr) {
            if(curr.key === key) {
                return curr.value;
            }

            curr = curr.next;
        }

        throw new Error('Such element does not exist in the table.');
    }

    /**
     * Remove a key-value pair from the hash table.
     * @param {string} key - The key to delete.
     * @throws {Error} If the key does not exist in the table.
     * Time complexity: O(1)
     */
    delete(key) {
        const index = hashFn(key, this.size);

        if(!this.#table[index]) {
            throw new Error('Such element does not exist in the table.');
        }

        let curr = this.#table[index];
        let prev = null;

        while(curr) {
            if(curr.key === key) {
                if(prev) {
                    prev.next = curr.next;
                } else {
                    this.#table[index] = null;
                }
                this.itemsCount--;

                return;
            }
            prev = curr;
            curr = curr.next;
        }

        throw new Error('Such element does not exist in the table.');
    }
}

module.exports = HashTable;