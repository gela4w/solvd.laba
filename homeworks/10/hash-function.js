module.exports = (str, size) => {
    let hash = 0;
    const prime = 31;

    for (let i = 0; i < str.length; i++) {
        hash = hash * prime + str.charCodeAt(i);
    }

    return hash % size;
};