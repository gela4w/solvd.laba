const HashTable = require('./hash-table');

describe('Hash Table', () => {
    const initialSize = 5;
    const phonebook = new HashTable(initialSize);
    phonebook.set('Julie', '+123 45 67 89');
    phonebook.set('John', '+123 65 49 87');
    phonebook.set('Mrs. Hudson', '+123 74 85 96');
    phonebook.set('William', '+123 85 76 94');
    phonebook.set('Zoe Blue', '+123 45 48 76');

    test('should insert key-value pairs in the table', () => {
        expect(phonebook.get('John')).toBe('+123 65 49 87');
        expect(phonebook.get('Mrs. Hudson')).toBe('+123 74 85 96');
    });

    test('should update the value in the table', () => {
        phonebook.set('Allie', '+123');

        expect(phonebook.get('Allie')).toBe('+123');

        phonebook.set('Allie', '+123 123');

        expect(phonebook.get('Allie')).toBe('+123 123');
    });

    test('should throw an error if table does not contain the key-value pair', () => {
        expect(() => phonebook.get('Sherlock')).toThrow('Such element does not exist in the table.');
    });

    test('should re-balance the table when fill factor is greater than 70%', () => {
        expect(phonebook.size).toBe(initialSize * 2);
    });

    test('should delete item from the table', () => {
        const itemsCount =  phonebook.itemsCount;

        phonebook.delete('William');

        expect(phonebook.itemsCount).toBe(itemsCount - 1);
        expect(() => phonebook.get('William')).toThrow('Such element does not exist in the table.');
    });

    test('should throw an error on attempt to delete an item that does not exist in the table', () => {
        expect(() => phonebook.delete('Sherlock')).toThrow('Such element does not exist in the table.');
    });
});