const {
    calculateDiscountedPrice,
    calculateTotalPrice,
    getFullName,
    filterUniqueWords,
    getAverageGrade,
    createCounter,
    repeatFunction,
    calculateFactorial,
    power,
    lazyMap,
    fibonacciGenerator
} = require('./functions');

describe('Functions', () => {
    describe('Immutability and Pure Functions', () => {
        test('should calculated discounted prices', () => {
            expect(calculateDiscountedPrice([10, 15, 30], 5)).toStrictEqual([9.5, 14.25, 28.5]);
            expect(calculateDiscountedPrice([10.25, 15.37, 30.18], 3)).toStrictEqual([9.94, 14.91, 29.27]);
            expect(calculateDiscountedPrice([10.25, 15.37, 30.18], 99)).toStrictEqual([0.1, 0.15, 0.3]);
        });

        test('should calculate the total price', () => {
            expect(calculateTotalPrice([10, 15, 30])).toBe(55);
            expect(calculateTotalPrice([10.25, 15.37, 30.18])).toBe(55.80);
            expect(calculateTotalPrice([])).toBe(0);
        });
    });

    describe('Function Composition and Point-Free Style', () => {
        test('should return person\'s full name', () => {
            expect(getFullName({ firstName: 'Angelina', lastName: 'Jolie' })).toBe('Angelina Jolie');
            expect(getFullName({})).toBe(' ');
        });

        test('should return an array of unique words sorted alphabetically', () => {
            expect(filterUniqueWords('A winter was cold and winter was snowy, and snowy.'))
                .toStrictEqual(['A', 'and', 'cold', 'snowy', 'was', 'winter']);
        });

        test('should return an average students\' grade', () => {
            const students = [
                { name: 'John', grades: [80, 75, 21, 36] },
                { name: 'Jane', grades: [64, 89, 100] },
                { name: 'Lilly', grades: [18, 77, 64, 39, 100] }
            ];

            expect(getAverageGrade(students)).toBe(66);
        });
    });

    describe('Closures and Higher-Order Functions', () => {
        test('should create a counter function each call of which should increment a counter by one', () => {
            const counter1 = createCounter();
            const counter2 = createCounter();

            expect(counter1()).toBe(1);
            expect(counter1()).toBe(2);
            expect(counter1()).toBe(3);
            expect(counter2()).toBe(1);
        });

        test('should call passed function n-times', () => {
            const fn = jest.fn();
            const func = repeatFunction(fn, 10);
            func();

            expect(fn).toHaveBeenCalledTimes(10);
        });
    });

    describe('Recursion and Tail Call Optimization', () => {
        test('should count factorial of a given number', () => {
            expect(calculateFactorial(5)).toBe(120);
            expect(calculateFactorial(20)).toBe(2_432_902_008_176_640_000);
        });

        test('should exponentiate a number', () => {
            expect(power(2, 3)).toBe(Math.pow(2, 3));
            expect(power(8, 16)).toBe(Math.pow(8, 16));
        });
    });

    describe('Lazy Evaluation and Generators', () => {
        test('should create a lazy generator that applies mapping function to each value in an array', () => {
            const generator = lazyMap([2, 3, 4, 5], el => el * 2);

            expect(generator.next()).toStrictEqual({ value: 4, done: false });
            expect(generator.next()).toStrictEqual({ value: 6, done: false });
            expect(generator.next()).toStrictEqual({ value: 8, done: false });
            expect(generator.next()).toStrictEqual({ value: 10, done: true });
            expect(generator.next()).toStrictEqual({ done: true });
            expect(generator.next()).toStrictEqual({ done: true });
        });

        test('should create a lazy generator that returns each number of Fibonacci sequence a time', () => {
            const generator = fibonacciGenerator();
            const generator2 = fibonacciGenerator();
            const generator3 = fibonacciGenerator();

            expect(generator.next()).toStrictEqual({ value: 1, done: false });
            expect(generator.next()).toStrictEqual({ value: 2, done: false });
            expect(generator.next()).toStrictEqual({ value: 3, done: false });
            expect(generator.next()).toStrictEqual({ value: 5, done: false });
            expect(generator.next()).toStrictEqual({ value: 8, done: false });
            expect(generator.next()).toStrictEqual({ value: 13, done: false });
            expect(generator2.next()).toStrictEqual({ value: 1, done: false });
            expect(generator3.next()).toStrictEqual({ value: 1, done: false });
            expect(generator3.next()).toStrictEqual({ value: 2, done: false });
        });
    });
});