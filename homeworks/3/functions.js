module.exports.calculateDiscountedPrice = (prices, discount) => {
    return prices.map(price => Number((price * ((100 - discount) / 100)).toFixed(2)));
};

module.exports.calculateTotalPrice = (products) => products.reduce((curr, acc) => curr + acc, 0);

module.exports.getFullName = (person) => `${person.firstName || ''} ${person.lastName || ''}`;

module.exports.filterUniqueWords = (string) => Array.from(new Set(string.replace(/[^\w\s]/g, '').split(' '))).sort();

module.exports.getAverageGrade = (students) => {
    Array.prototype.getAverage = function() {
        return Math.round(this.reduce((curr, acc) => curr + acc) / this.length);
    }

    return students.map(({ grades }) => grades.getAverage()).getAverage();
};

module.exports.createCounter = () => {
    let count = 0;
    return () => ++count;
};

module.exports.repeatFunction = (fn, calls) => {
    if(calls <= 0) {
        return () => {
            while(true) { fn(); }
        }
    } else {
        return () => {
            let count = calls;
            while(count > 0) {
                fn();
                count--;
            }
        }
    }
};

module.exports.calculateFactorial = function calculateFactorial(num, acc = 1) {
    return num === 0 ? acc: calculateFactorial(num  - 1, num * acc);
};

module.exports.power = function power(base, pow) {
    return pow === 1 ? base : base * power(base, pow - 1);
};

module.exports.lazyMap = function lazyMap(arr, fn) {
    let ind = 0;
    const getResult = () => {
        if(ind < arr.length) {
            return {
                value: fn(arr[ind++]),
                done: ind === arr.length
            };
        }
        return { done: true };
    }

    return { next: getResult };
};

module.exports.fibonacciGenerator = function fibonacciGenerator() {
    let prev = 0;
    let curr = 1;
    const getNumber = () => {
        let sum = prev + curr;
        prev = curr;
        curr = sum;

        return { value: sum, done: false };
    }
    return { next: getNumber };
};