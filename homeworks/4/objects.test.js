const {
    person,
    deleteNonConfigurable,
    getTotalPrice,
    product,
    BankAccount,
    createImmutableObject,
    observeObject,
    deepCloneObject,
    validateObject
} = require('./objects');

describe('Objects', () => {
    function getTestObject() {
        return {
            _firstName: 'Peter',
            _lastName: 'Parker',
            age: 28,
            food: ['apples', 'bananas'],
            address: { country: 'USA', addr: { city: 'NYC', code: '123', info: { districts: 5, tel: '+748' } } },
            get fullName() { return this._firstName + ' ' +  this._lastName },
            set fullName(val) { [this._firstName, this._lastName] = val.split(' ')}
        }
    }

    describe('Object Property Manipulations', () => {
        test('should set person\'s properties descriptors', () => {
            const descriptors = Object.getOwnPropertyDescriptors(person);

            expect(descriptors['firstName'].writable).toBeFalsy();
            expect(descriptors['lastName'].writable).toBeFalsy();
            expect(descriptors['age'].writable).toBeFalsy();
            expect(descriptors['email'].writable).toBeFalsy();
            expect(descriptors['address'].configurable).toBeFalsy();
            expect(descriptors['address'].enumerable).toBeFalsy();
        });

        test('should update person\'s properties and keep descriptors specified', () => {
            const info = { firstName: 'Peter', lastName: 'Parker', unexpectedProp: 'test' };
            person.updateInfo(info);

            expect(person.firstName).toBe('Peter');
            expect(Object.getOwnPropertyDescriptor(person, 'firstName').writable).toBeFalsy();
            expect(person.lastName).toBe('Parker');
            expect(Object.getOwnPropertyDescriptor(person, 'lastName').writable).toBeFalsy();
            expect(person.age).toBe('30');
            expect(person.email).toBe('john.doe@example.com');
            expect(person.unexpectedProp).not.toBeDefined();
        });
    });

    describe('Object Property Enumeration and Deletion', () => {
        test('should set product\'s  properties descriptors', () => {
            const descriptors = Object.getOwnPropertyDescriptors(product);

            expect(descriptors['price'].writable).toBeFalsy();
            expect(descriptors['price'].enumerable).toBeFalsy();
            expect(descriptors['quantity'].writable).toBeFalsy();
            expect(descriptors['quantity'].enumerable).toBeFalsy();
        });

        test('should return products total price', () => {
            expect(getTotalPrice(product)).toBe(5000);
        });

        describe('DeleteNonConfigurable', () => {
            const object = { prop1: 'value1', prop2: 'value2' };
            Object.defineProperty(object, 'prop2', { configurable: false });

            test('should delete configurable property from the object', () => {
                const obj = { ... object };
                deleteNonConfigurable(obj, 'prop1');

                expect(obj).toStrictEqual({ prop2: 'value2' });
            });

            test('should throw an error if specified property does not exist in object', () => {
                expect(() => deleteNonConfigurable(object, 'test'))
                    .toThrow('Property "test" does not exist in the object.');
            });

            test('should throw an error for deletion of a non-configurable property', () => {
                expect(() => deleteNonConfigurable(object, 'prop2'))
                    .toThrow('Property "prop2" could not be deleted from the object.');
            });
        });
    });

    describe('Object Property Getters and Setters', () => {
        test('should return formatted account balance', () => {
            const firstAccount = new BankAccount(1000);
            const secondAccount = new BankAccount(500);

            expect(firstAccount.formattedBalance).toBe('$1000');
            expect(secondAccount.formattedBalance).toBe('$500');
        });

        test('should update account balance', () => {
            const bankAccount = new BankAccount(1000);

            expect(bankAccount.formattedBalance).toBe('$1000');

            bankAccount.balance = 222;

            expect(bankAccount.formattedBalance).toBe('$222');
        });

        describe('Transfer', () => {
            test('should transfer funds from one account to another', () => {
                const firstAccount = new BankAccount(1000);
                const secondAccount = new BankAccount(500);

                firstAccount.transfer(secondAccount, 55);

                expect(firstAccount.formattedBalance).toBe('$945');
                expect(secondAccount.formattedBalance).toBe('$555');

                secondAccount.transfer(firstAccount, 100);

                expect(firstAccount.formattedBalance).toBe('$1045');
                expect(secondAccount.formattedBalance).toBe('$455');
            });

            test('should throw an error if transfer amount is more than account balance', () => {
                const bankAccount = new BankAccount(1000);

                expect(() => bankAccount.transfer({}, 10000))
                    .toThrow('The operation cannot be completed. Insufficient funds in the account.');
            });
        });
    });

    describe('Advanced Property Descriptors', () => {
        const sourceObj = getTestObject();
        sourceObj.food.push({ veg1: 'potato', veg2: 'tomato' }, [ 'milk', 'yogurt']);
        const copy = createImmutableObject(sourceObj);

        test('should create independent copies of nested objects', () => {
            sourceObj.food.push('kiwi');
            sourceObj.food[2]['veg3'] = 'cucumber';
            sourceObj.address.addr.city = 'LA';
            sourceObj.address.addr.info.districts = 8;

            expect(sourceObj.food.length).toBe(5);
            expect(copy.food.length).toBe(4);

            expect(sourceObj.food[2]).toStrictEqual({ veg1: 'potato', veg2: 'tomato', veg3: 'cucumber' });
            expect(copy.food[2]).toStrictEqual({ veg1: 'potato', veg2: 'tomato' });

            expect(sourceObj.address.addr.city).toBe('LA');
            expect(copy.address.addr.city).toBe('NYC');

            expect(sourceObj.address.addr.info.districts).toBe(8);
            expect(copy.address.addr.info.districts).toBe(5);
        });

        test('should make copy\'s properties read-only', () => {
           copy.fullName = 'Jane Doe';
           copy.food = ['milk', 'porridge'];
           copy.address.addr.city = 'San Francisco';

           expect(copy.fullName).toBe('Peter Parker');
           expect(copy.food.length).toBe(4);
           expect(copy.food[0]).toBe('apples');
           expect(copy.address.addr.city).toBe('NYC');
        });
    });

    describe('Object Observation', () => {
        test('should execute callback function every time object\'s property is accessed or modified', () => {
            const obj = getTestObject();
            const cb = jest.fn();
            const proxy = observeObject(obj, cb);

            proxy.fullName;
            expect(cb).toHaveBeenCalledWith('fullName', 'get');

            proxy.fullName = 'Jane Doe';
            expect(cb).toHaveBeenCalledWith('fullName', 'set');

            proxy.age = 20;
            expect(cb).toHaveBeenCalledWith('age', 'set');
        });
    });

    describe('Object Deep Cloning',() => {
        const obj = {
            a: '1',
            b: { c: { d: 2, f: 3 } },
            g: ['4', 5, [6, 7, { h: 8 }], { i: 9 }],
        };
        obj.circular = obj;

        const copy = deepCloneObject(obj);

        test('should correctly copy nested objects', () => {
           expect(copy.b).toStrictEqual({ c: { d: 2, f: 3 } });

           obj.b.c.d = 'test';

           expect(obj.b.c.d).toBe('test');
           expect(copy.b.c.d).toBe(2);
        });

        test('should correctly copy nested arrays', () => {
            expect(copy.g).toStrictEqual(['4', 5, [6, 7, { h: 8 }], { i: 9 }]);

            obj.g = ['new', 'value', 'in', 'obj'];

            expect(obj.g[2]).toBe('in');
            expect(copy.g[2]).toStrictEqual([6, 7, { h: 8 }]);
        });

        test('should correctly handle circular dependencies', () => {
           obj.a = 'test';

           expect(copy.circular).toStrictEqual(copy);

           copy.a = 'abracadabra';

           expect(copy.circular.a).toBe('abracadabra');
        });
    });

    describe('Object Property Validation', () => {
        const minLength = (length) => (val) => val && val.length >= length;
        const isPositiveNumber = val => val > 0;
        const isStringInArray = val => val.every(el => typeof el === 'string');
        const isPhone = val => val.startsWith('+');

        const addressSchema = {
            type: 'object',
            props: {
                country: { type: 'string', validators: [minLength(2)] },
                addr: {
                    type: 'object',
                    props: {
                        city: { type: 'string', validators: [minLength(2)] },
                        code: { type: 'string', validators: [minLength(3)] },
                        info: {
                            type: 'object',
                            props: {
                                districts: { type: 'number', validators: [isPositiveNumber] },
                                tel: { type: 'string', validators: [isPhone, minLength(3)] },
                            },
                        },
                    },
                },
            },
        };
        const schema = {
            _firstName: { type: 'string', validators: [minLength(2)] },
            _lastName: { type: 'string', validators: [minLength(2)] },
            age: { type: 'number', validators: [isPositiveNumber] },
            food: { type: 'array', validators: [isStringInArray] },
            address: addressSchema,
            fullName: { type: 'string' },
        };

        test('should return true if object matches the schema', () => {
            expect(validateObject(getTestObject(), schema)).toBeTruthy();
        });

        test('should return false if object does not match the schema', () => {
            const obj1 = getTestObject();
            obj1.food.push({ potato: '1kg' });

            expect(validateObject(obj1, schema)).toBeFalsy();

            const obj2 = getTestObject();
            obj2.address.addr.info.tel = '+7';

            expect(validateObject(obj2, schema)).toBeFalsy();

            const obj3 = getTestObject();
            obj3.address = 123;

            expect(validateObject(obj3, schema)).toBeFalsy();
        });
    });
});