// Task 1: Object Property Manipulation
const person = Object.defineProperties({}, {
    firstName: { value: 'John', configurable: true, enumerable: true },
    lastName: { value: 'Doe', configurable: true, enumerable: true },
    age: { value: '30', configurable: true, enumerable: true },
    email: { value: 'john.doe@example.com', configurable: true, enumerable: true },
    address: { value: {}, writable: true },
});

person.updateInfo = function(info) {
    Object.entries(info).filter(([ key ]) => key in this).forEach(([ key, value]) => {
        Object.defineProperty(this, key, { value });
    });
};

module.exports.person = person;

// Task 2: Object Property Enumeration and Deletion
module.exports.product = Object.defineProperties({}, {
    name: { value: 'Laptop', writable: true, configurable: true, enumerable: true },
    price: { value: 1000, configurable: true },
    quantity: { value: 5, configurable: true },
});

module.exports.getTotalPrice = (product) => {
    // don't get the necessity of getOwnPropertyDescriptor usage,
    // as enumerability refers to accessing properties within for...in loop or through Object.keys(),
    // so this syntax also works:
    // product.price * product.quantity;

    return Object.getOwnPropertyDescriptor(product, 'price').value
        * Object.getOwnPropertyDescriptor(product, 'quantity').value;
};

module.exports.deleteNonConfigurable = (obj, prop) => {
    if(!obj.hasOwnProperty(prop)) {
        throw new Error(`Property "${prop}" does not exist in the object.`);
    } else if(!Object.getOwnPropertyDescriptor(obj, prop).configurable) {
        throw new Error(`Property "${prop}" could not be deleted from the object.`);
    } else {
        delete obj[prop];
    }
};

// Task 3: Object Property Getters and Setters
module.exports.BankAccount = class BankAccount {
    #balance = 0;
    currency = '$';

    constructor(initialBalance) {
        this.#balance = initialBalance;
    }

    get formattedBalance() {
        return `${this.currency}${this.#balance}`;
    };

    set balance(amount) {
        this.#balance= amount;
    };

    increaseBalance(amount) {
        this.balance = this.#balance + amount;
    };

    decreaseBalance(amount) {
        if(amount > this.#balance) {
            throw new Error('The operation cannot be completed. Insufficient funds in the account.');
        }
        this.balance = this.#balance - amount;
    };

    transfer(recipient, amount) {
        this.decreaseBalance(amount);
        recipient.increaseBalance(amount);
    };
};

// Task 4: Advanced Property Descriptors
function createImmutableObject(obj) {
    const copy = Array.isArray(obj) ? [] : {};

    for(let key in obj) {
        let value = obj[key];

        if(typeof value === 'object') {
            value = createImmutableObject(value);
        }
        Object.defineProperty(copy, key, { value, enumerable: true });
    }

    return copy;
}

const immutablePerson = createImmutableObject(person);

module.exports.createImmutableObject = createImmutableObject;

// Task 5: Object Observation
function observeObject(obj, fn) {
    // return new Proxy(obj, {
    //     get(target, prop, receiver) {
    //         fn(prop, 'get');
    //         return Reflect.get(target, prop, receiver);
    //     },
    //     set(target, prop, val, receiver) {
    //         fn(prop, 'set');
    //         return Reflect.set(target, prop, val, receiver);
    //     },
    //     deleteProperty(target, prop) {
    //         fn(prop, 'delete');
    //         return Reflect.deleteProperty(target, prop);
    //     }
    // });
    let proxy = {};

    for(let key in obj) {
        Object.defineProperty(proxy, key, {
            get() {
                fn(key, 'get');
                return obj[key];
            },
            set(val) {
                fn(key, 'set');
                obj[key] = val;
            },
            configurable: true,
            enumerable: true
        });
    }

    return proxy;
}

module.exports.observeObject = observeObject;
module.exports.proxyPerson = observeObject(person, (prop, operation) => console.log(prop, operation));

// Task 6: Object Deep Cloning
module.exports.deepCloneObject = function deepCloneObject(obj, clonedObjs = new WeakMap()) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }

    if (clonedObjs.has(obj)) {
        return clonedObjs.get(obj);
    }

    const clone = Array.isArray(obj) ? [] : {};

    clonedObjs.set(obj, clone);

    for (let key in obj) {
        clone[key] = deepCloneObject(obj[key], clonedObjs);
    }

    return clone;
};

// Task 7: Object Property Validation
module.exports.validateObject = function validateObject(obj, schema) {
    let isTypeCorrect = false;

    for(let key in obj) {
        let _value = obj[key];
        let _schema = schema[key];

        isTypeCorrect = _schema.type === 'array' ? Array.isArray(_value) : typeof _value === _schema.type;

        if(!isTypeCorrect) return false;

        if(typeof _value === 'object' && !Array.isArray(_value)) {
            if(!validateObject(_value, _schema.props)) return false;
        }

        if(_schema.validators) {
            const validationResults = _schema.validators.map(validator => validator(_value));

            if(validationResults.some(res => !res)) return false;
        }
    }

    return true;
};