require('./1-string-arithmetic-operations.js');

describe('Addition', () => {
    test('should add two numbers', () => {
        let op1 = 123, op2 = 456;

        expect(op1.toString().plus(op2.toString())).toBe(String(op1 + op2));
    });

    test('should add two numbers with overflow', () => {
        let op1 = 9999, op2 = 111;

        expect(op1.toString().plus(op2.toString())).toBe(String(op1 + op2));
    });

    test('should add big numbers', () => {
        let op1 = '999999999999999999999999999999999999999', op2 = '9999999999999999999';

        expect(op1.plus(op2)).toBe('1000000000000000000009999999999999999998');
    });
});

describe('Subtraction', () => {
    test('should subtract one number from another', () => {
        let op1 = 1000, op2 = 2;

        expect(op1.toString().minus(op2.toString())).toBe(String(op1 - op2));
    });

    test('should subtract one number from another with overflow', () => {
        let op1 = 1234, op2 = 758;

        expect(op1.toString().minus(op2.toString())).toBe(String(op1 - op2));
    });

    test('should trim unnecessary zeroes from result', () => {
        let op1 = 1111, op2 = 999;

        expect(op1.toString().minus(op2.toString())).toBe(String(op1 - op2));
    });

    test('should show zero as a result', () => {
        let op1 = 1, op2 = 1;

        expect(op1.toString().minus(op2.toString())).toBe(String(op1 - op2));
    });

    test('should subtract big numbers', () => {
        let op1 = '999999999999999999999999999999999999999', op2 = '9999999999999999999';

        expect(op1.minus(op2)).toBe('999999999999999999990000000000000000000');
    });

    test('should throw an error if subtrahend is greater than minuend', () => {
        let op1 = '123', op2 = '456';

        expect(() => op1.minus(op2)).toThrow('Forbidden operation');
    });
});

describe('Multiplication', () => {
    test('should multiply by one digit number', () => {
        let op1 = 123, op2 = 2;

        expect(op1.toString().multiply(op2.toString())).toBe(String(op1 * op2));
    });

    test('should multiply by multi-digit number', () => {
        let op1 = 123, op2 = 456789;

        expect(op1.toString().multiply(op2.toString())).toBe(String(op1 * op2));
    });

    test('should multiply by zero', () => {
        let op1 = 123, op2 = 0;

        expect(op1.toString().multiply(op2.toString())).toBe(String(op1 * op2));
    });
});

describe('Division', () => {
    test('should divide two numbers', () => {
        let op1 = 100, op2 = 2;

        expect(op1.toString().divide(op2.toString())).toBe(String(op1 / op2));
    });

    test('should return only integer value of division', () => {
        let op1 = 1234, op2 = 75;

        expect(op1.toString().divide(op2.toString())).toBe(String(Math.floor(op1 / op2)));
    });

    test('should return zero if dividend is less than divider', () => {
        let op1 = 2, op2 = 100;

        expect(op1.toString().divide(op2.toString())).toBe('0');
    });

    test('should handle division by zero', () => {
        let op1 = 123, op2 = 0;

        expect(op1.toString().divide(op2.toString())).toBe('Infinity');
    });
});