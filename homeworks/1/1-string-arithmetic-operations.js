//Helpers
String.prototype.isEqualTo = function(str) {
    if(this.length !== str.length) {
        return false;
    }

    for(let i = 0; i < this.length; i++) {
        if(this[i] !== str[i]) {
            return false;
        }
    }

    return true;
}

String.prototype.isGreaterThan = function(str) {
    if(this.length > str.length) {
        return true;
    }

    if(this.length < str.length) {
        return false;
    }

    for(let i = 0; i < this.length; i++) {
        if(this[i] !== str[i]) {
            return this[i] > str[i];
        }
    }

    return false;
}

String.prototype.multiplyBySingleDigit = function(digit) {
    let carry = 0;
    let res = '';

    if(digit === '0') {
        return '0';
    }

    for(let i = this.length - 1; i >= 0; i--) {
        let num = this[i] * digit + carry;
        res = (num % 10) + res;
        carry = Math.floor(num / 10);
    }

    return carry > 0 ? carry + res : res;
}

//Implementations
String.prototype.plus = function(str){
    let carry = 0;
    let res = '';
    let op1 = this.split('').reverse();
    let op2 = str.split('').reverse();

    let length = Math.max(op1.length, op2.length);

    for(let i = 0; i < length; i++) {
        let sum = (+op1[i] || 0) + (+op2[i] || 0) + carry;
        res = (sum % 10) + res;
        carry = Math.floor(sum / 10);
    }

    return carry > 0 ? carry + res : res;
}

String.prototype.minus = function(subtrahend) {
    let borrow = 0;
    let res = '';
    let op1 = this.split('').reverse();
    let op2 = subtrahend.split('').reverse();

    if(subtrahend.isGreaterThan(this)) {
        throw new Error('Forbidden operation');
    }

    for(let i = 0; i < this.length; i++) {
        let interRes = op1[i] - borrow - (op2[i] || 0);
        if(interRes < 0) {
            interRes += 10;
            borrow = 1;
        } else {
            borrow = 0;
        }
        res = interRes + res;
    }

    return res.replace(/^0+/, '') || '0';
}

String.prototype.multiply = function(multiplier) {
    let res = '0';

    for(let i = multiplier.length - 1; i >=0; i--) {
        const interRes = this.multiplyBySingleDigit(multiplier[i]) + '0'.repeat(multiplier.length - i - 1);
        res = res.plus(interRes);
    }

    return res;
}

String.prototype.divide = function(divider) {
    let dividend = this;
    let res = '0';

    if(divider === '0') {
        return 'Infinity';
    }

    while(dividend.isGreaterThan(divider) || dividend.isEqualTo(divider)) {
        let exponential = dividend.length - divider.length;
        let multipliedDivider = divider.multiply('1' + '0'.repeat(exponential));

        if(multipliedDivider.isGreaterThan(dividend)) {
            exponential--;
            multipliedDivider = divider.multiply('1' + '0'.repeat(exponential));
        }

        dividend = dividend.minus(multipliedDivider);
        res = res.plus('1' + '0'.repeat(exponential));
    }

    return res;
}
