const Queue = require('./queue');
const Stack = require('./stack');

/** Class representing a graph data structure. */
class Graph {
    /**
     * Create a graph.
     */
    constructor() {
        this.adjacencyList = {};
    }

    /**
     * Add a vertex to the graph.
     * @param {*} vertex - The vertex to be added.
     * Time complexity: O(1)
     */
    addVertex(vertex) {
        if(!this.adjacencyList[vertex]) {
            this.adjacencyList[vertex] = [];
        }
    }

    /**
     * Add an edge between two vertices in the graph.
     * @param {*} vertex1 - The first vertex.
     * @param {*} vertex2 - The second vertex.
     * @throws Will throw an error if one or both vertices do not exist.
     * Time complexity: O(1)
     */
    addEdge(vertex1, vertex2) {
        if(!this.adjacencyList[vertex1] || !this.adjacencyList[vertex2]) {
            throw new Error('One or both vertices do not exist.');
        }

        this.adjacencyList[vertex1].push(vertex2);
        this.adjacencyList[vertex2].push(vertex1);
    }

    /**
     * Perform a breadth-first search starting from a given vertex.
     * @param {*} start - The starting vertex.
     * @return {Array} The vertices visited in the BFS order.
     * @throws Will throw an error if the start vertex does not exist.
     * Time complexity: O(V + E)
     */
    breadthFirstSearch(start) {
        if(!this.adjacencyList[start]) {
            throw new Error('The start vertex does not exist.');
        }

        const visited = [];
        const queue = new Queue();

        queue.enqueue(start);

        while(!queue.isEmpty) {
            const vertex = queue.dequeue();

            if(!visited.includes(vertex)) {
                visited.push(vertex);
                this.adjacencyList[vertex].forEach(neighbour => queue.enqueue(neighbour));
            }
        }

        return visited;
    }

    /**
     * Perform a depth-first search starting from a given vertex.
     * @param {*} start - The starting vertex.
     * @returns {Array} The vertices visited in the DFS order.
     * @throws Will throw an error if the start vertex does not exist.
     * Time complexity: O(V + E)
     */
    depthFirstSearch(start) {
        if(!this.adjacencyList[start]) {
            throw new Error('The start vertex does not exist.');
        }

        const visited = [];
        const stack = new Stack();

        stack.push(start);

        while(!stack.isEmpty) {
            const vertex = stack.pop();

            if(!visited.includes(vertex)) {
                visited.push(vertex);
                this.adjacencyList[vertex].forEach(neighbour => stack.push(neighbour));
            }
        }

        return visited;
    }
}

module.exports = Graph;
