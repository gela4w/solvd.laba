const LinkedList = require('./singly-linked-list');

describe('Singly Linked List Data Structure', () => {
    const getListWithItems = () => {
        const list = new LinkedList();

        list.insert('value 1', 0);
        list.insert('value 2', 1);
        list.insert('value 3', 2);
        list.insert('value 4', 3);

        return list;
    };

    describe('Insert', () => {
        test('should add elements to the list', () => {
            const list = getListWithItems();

            expect(list.get(0)).toBe('value 1');
            expect(list.get(1)).toBe('value 2');
            expect(list.get(2)).toBe('value 3');
            expect(list.get(3)).toBe('value 4');
        });

        test('should add element to the beginning of the list', () => {
            const list = getListWithItems();

            list.insert('FIRST', 0);

            expect(list.get(0)).toBe('FIRST');
            expect(list.get(1)).toBe('value 1');
            expect(list.get(2)).toBe('value 2');
            expect(list.get(3)).toBe('value 3');
            expect(list.get(4)).toBe('value 4');
        });

        test('should add element to the end of the list', () => {
            const list = getListWithItems();

            list.insert('LAST', 4);

            expect(list.get(0)).toBe('value 1');
            expect(list.get(1)).toBe('value 2');
            expect(list.get(2)).toBe('value 3');
            expect(list.get(3)).toBe('value 4');
            expect(list.get(4)).toBe('LAST');
        });

        test('should add element to the middle of the list', () => {
            const list = getListWithItems();

            list.insert('NEW ELEMENT', 2);

            expect(list.get(0)).toBe('value 1');
            expect(list.get(1)).toBe('value 2');
            expect(list.get(2)).toBe('NEW ELEMENT');
            expect(list.get(3)).toBe('value 3');
            expect(list.get(4)).toBe('value 4');
        });

        test('should throw an error if an incorrect index was provided', () => {
            const list = getListWithItems();

            expect(() => list.insert('value 5', -1))
                .toThrow('Item cannot be inserted. -1 is an incorrect index.');

            expect(() => list.insert('value 6', 5))
                .toThrow('Item cannot be inserted. 5 is an incorrect index.');
        });
    });

    describe('Remove', () => {
        test('should remove the first element from the list', () => {
            const list = getListWithItems();

            expect(list.remove(0)).toBe('value 1');
        });

        test('should remove the last element from the list', () => {
            const list = getListWithItems();

            expect(list.remove(3)).toBe('value 4');
        });

        test('should remove the element from the middle of the list', () => {
            const list = getListWithItems();

            expect(list.remove(2)).toBe('value 3');
        });

        test('should throw an error if an incorrect index was provided', () => {
            const list = getListWithItems();

            expect(() => list.remove(-1))
                .toThrow('Item cannot be removed. -1 is an incorrect index.');

            expect(() => list.remove( 4))
                .toThrow('Item cannot be removed. 4 is an incorrect index.');
        });

        test('should throw an error if list is empty', () => {
            const list = new LinkedList();

            expect(() => list.remove(0)).toThrow('The list is empty');
        });
    });

    describe('Find', () => {
        const list = getListWithItems();

        test('should return an index for the passed value', () => {
            expect(list.findIndexByValue('value 1')).toBe(0);
            expect(list.findIndexByValue('value 3')).toBe(2);
            expect(list.findIndexByValue('value 4')).toBe(3);
        });

        test('should throw an error if there is no such element in the list', () => {
            expect(() => list.findIndexByValue('no such value'))
                .toThrow('There is no such element in the list.');
        });
    });
});