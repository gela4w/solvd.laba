const dijkstra = require('./dijkstra');

describe('Dijkstra\'s Algorithm', () => {
    test('should find the shortest path between two vertices in the weighted graph', () => {
        const graph = {
            A: { B: 1, C: 4 },
            B: { A: 1, C: 2, D: 5 },
            C: { A: 4, B: 2, D: 1 },
            D: { B: 5, C: 1 },
        };

        expect(dijkstra(graph, 'A', 'D'))
            .toStrictEqual({ distance: 4, path: ['A', 'B', 'C', 'D'] });

        expect(dijkstra(graph, 'B', 'D'))
            .toStrictEqual({ distance: 3, path: ['B', 'C', 'D'] });
    });
});