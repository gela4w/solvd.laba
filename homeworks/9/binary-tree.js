/** Class representing a node in the binary tree. */
class Node {
    /**
     * Create a new node.
     * @param {*} val - The value to be stored in the node.
     */
    constructor(val) {
        this.val = val;
        this.left = null;
        this.right = null;
    }
}

/** Class representing a binary search tree data structure. */
class BinarySearchTree {
    /**
     * Create a binary search tree.
     */
    constructor() {
        this.root = null;
    }

    /**
     * Insert a value into the binary search tree.
     * @param {*} val - The value to be inserted.
     * Time complexity: O(log(n))
     */
    insert(val) {
        const node = new Node(val);

        if(this.root === null) {
            this.root = node;

            return;
        }

        let curr = this.root;

        while(curr) {
            if(curr.val > node.val) {
                if(curr.left === null) {
                    curr.left = node;

                    return;
                }
                curr = curr.left;
            } else {
                if(curr.right === null) {
                    curr.right = node;

                    return;
                }
                curr = curr.right;
            }
        }
    }

    /**
     * Find a value in the binary search tree.
     * @param {*} val - The value to find.
     * @return {Node} The node containing the value.
     * @throws {Error} If the value is not found or the tree is empty.
     * Time complexity: O(log(n))
     */
    find(val) {
        if(this.root === null) {
            throw new Error('The tree is empty.');
        }

        let curr = this.root;

        while(curr) {
            if(curr.val === val) {
                return curr;
            } else {
                if(curr.val > val) {
                    if(curr.left === null) {
                        throw new Error('There is no such value in the tree.');
                    }
                    curr = curr.left;
                } else {
                    if(curr.right === null) {
                        throw new Error('There is no such value in the tree.');
                    }
                    curr = curr.right;
                }
            }
        }
    }

    /**
     * Perform a pre-order(moving from the root to leafs) traversal of the binary search tree.
     * @return {Array} The values visited during the traversal.
     * Time complexity: O(n)
     */
    preOrder() {
        let visited = [];
        let curr = this.root;

        let traverse = node => {
            visited.push(node.val);

            if(node.left) traverse(node.left);

            if(node.right) traverse(node.right);
        };

        traverse(curr);

        return visited;
    }

    /**
     * Perform a post-order(moving from leafs to the root) traversal of the binary search tree.
     * @return {Array} The values visited during the traversal.
     * Time complexity: O(n)
     */
    postOrder() {
        let visited = [];
        let curr = this.root;

        let traverse = node => {
            if(node.left) traverse(node.left);

            if(node.right) traverse(node.right);

            visited.push(node.val);
        };

        traverse(curr);

        return visited;
    }

    /**
     * Perform an in-order(the left side then the right) traversal of the binary search tree.
     * @return {Array} The values visited during the traversal.
     * Time complexity: O(n)
     */
    inOrder() {
        let visited = [];
        let curr = this.root;

        let traverse = node => {
            if(node.left) traverse(node.left);

            visited.push(node.val);

            if(node.right) traverse(node.right);
        };

        traverse(curr);

        return visited;
    }
}

module.exports = BinarySearchTree;