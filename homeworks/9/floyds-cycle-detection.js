// Time complexity: O(n)
module.exports = node => {
    let slowPointer = node;
    let fastPointer = node;

    while(slowPointer !== null && fastPointer !== null && fastPointer.next !== null) {
        slowPointer = slowPointer.next;
        fastPointer = fastPointer.next.next;

        if(slowPointer === fastPointer) return true;
    }

    return false;
};
