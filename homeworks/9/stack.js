/** Class representing a node in the stack. */
class Node {
    /**
     * Create a new node.
     * @param {*} val - The value to be stored in the node.
     */
    constructor(val) {
        this.val = val;
        this.next = null;
    }
}

/** Class representing a stack data structure. */
class Stack {
    /**
     * Create a stack.
     */
    constructor() {
        this.top = null;
        this.mins = [];
        this.maxs = [];
    }

    /**
     * Check if the stack is empty.
     * @type {boolean}
     * Time complexity: O(1)
     */
    get isEmpty() {
        return this.top === null;
    }

    /**
     * Push a value onto the stack. Keep track of the smallest/largest values.
     * @param {*} val - The value to be pushed onto the stack.
     * Time complexity: O(1)
     */
    push(val) {
        const node = new Node(val);

        if(this.isEmpty) {
            this.top = node;
            this.mins.push(val);
            this.maxs.push(val);
        } else {
            node.next = this.top;
            this.top = node;

            if(val <= this.getMin()) {
                this.mins.push(val);
            }

            if(val >= this.getMax()) {
                this.maxs.push(val);
            }
        }
    }

    /**
     * Pop the top value off the stack. Keep track of the smallest/largest values.
     * @return {*} The value popped from the stack.
     * @throws {Error} If the stack is empty.
     * Time complexity: O(1)
     */
    pop() {
        if(this.isEmpty) {
            throw new Error('The stack is empty.');
        }

        let top = this.top;
        let prev = top.next;

        top.next = null;
        this.top = prev;

        if(this.isEmpty || top.val === this.getMin()) {
            this.mins.pop();
        }

        if(this.isEmpty || top.val === this.getMax()) {
            this.maxs.pop();
        }

        return top.val;
    }

    /**
     * Return the top value of the stack without removing it.
     * @return {*} The value at the top of the stack.
     * @throws {Error} If the stack is empty.
     * Time complexity: O(1)
     */
    peek() {
        if(this.isEmpty) {
            throw new Error('The stack is empty.');
        }

        return this.top.val;
    }

    /**
     * Get the smallest value in the stack.
     * @return {*} The smallest value in the stack.
     * @throws {Error} If the stack is empty.
     * Time complexity: O(1)
     */
    getMin() {
        if(this.isEmpty) {
            throw new Error('The stack is empty.');
        }

        return this.mins[this.mins.length - 1];
    }

    /**
     * Get the largest value in the stack.
     * @return {*} The largest value in the stack.
     * @throws {Error} If the stack is empty.
     * Time complexity: O(1)
     */
    getMax() {
        if(this.isEmpty) {
            throw new Error('The stack is empty.');
        }

        return this.maxs[this.maxs.length - 1];
    }
}

module.exports = Stack;