const Graph = require('./graph');
const bfs = require('./bfs');

describe('Breadth-First Search', () => {
    test('should find the shortest path between two vertices in the unweighted graph', () => {
        const graph = new Graph();

        graph.addVertex('A');
        graph.addVertex('B');
        graph.addVertex('C');
        graph.addVertex('D');
        graph.addVertex('E');
        graph.addVertex('F');
        graph.addVertex('G');

        graph.addEdge('A', 'B');
        graph.addEdge('A', 'E');
        graph.addEdge('B', 'C');
        graph.addEdge('C', 'D');
        graph.addEdge('C', 'F');
        graph.addEdge('C', 'G');
        graph.addEdge('E', 'F');
        graph.addEdge('E', 'G');

        expect(bfs(graph, 'A', 'D')).toStrictEqual(['A', 'B', 'C', 'D']);
        expect(bfs(graph, 'B', 'G')).toStrictEqual(['B', 'C', 'G']);
        expect(() => bfs(graph, 'A', 'X'))
            .toThrow('There is no such path in the graph.');
    });
});