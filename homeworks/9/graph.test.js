const Graph = require('./graph');

const getGraph1 = () => {
    const graph = new Graph();

    graph.addVertex('A');
    graph.addVertex('B');
    graph.addVertex('C');
    graph.addVertex('D');
    graph.addVertex('E');

    graph.addEdge('A', 'B');
    graph.addEdge('A', 'C');
    graph.addEdge('A', 'D');
    graph.addEdge('B', 'C');
    graph.addEdge('C', 'D');
    graph.addEdge('C', 'E');

    return graph;
}

const getGraph2 = () => {
    const graph = new Graph();

    graph.addVertex('A');
    graph.addVertex('B');
    graph.addVertex('C');
    graph.addVertex('D');
    graph.addVertex('E');

    graph.addEdge('A', 'B');
    graph.addEdge('A', 'C');
    graph.addEdge('A', 'D');
    graph.addEdge('B', 'C');
    graph.addEdge('C', 'D');

    return graph;
}

describe('Graph', () => {
    const graph1 = getGraph1();
    const graph2 = getGraph2();

    test('should traverse the graph using the breadth-first search', () => {
        expect(graph1.breadthFirstSearch('A')).toStrictEqual(['A', 'B', 'C', 'D', 'E']);
        expect(graph1.breadthFirstSearch('B')).toStrictEqual(['B', 'A', 'C', 'D', 'E']);
        expect(graph2.breadthFirstSearch('A')).toStrictEqual(['A', 'B', 'C', 'D']);
        expect(graph2.breadthFirstSearch('D')).toStrictEqual(['D', 'A', 'C', 'B']);
        expect(graph2.breadthFirstSearch('E')).toStrictEqual(['E']);

        expect(() => graph1.breadthFirstSearch('X'))
            .toThrow('The start vertex does not exist.');
    });

    test('should traverse the graph using the depth-first search', () => {
        expect(graph1.depthFirstSearch('A')).toStrictEqual(['A', 'D', 'C', 'E', 'B']);
        expect(graph1.depthFirstSearch('B')).toStrictEqual(['B', 'C', 'E', 'D', 'A']);
        expect(graph2.depthFirstSearch('A')).toStrictEqual(['A', 'D', 'C', 'B']);
        expect(graph2.depthFirstSearch('D')).toStrictEqual(['D', 'C', 'B', 'A']);
        expect(graph2.depthFirstSearch('E')).toStrictEqual(['E']);

        expect(() => graph1.depthFirstSearch('X'))
            .toThrow('The start vertex does not exist.');
    });
});