// Time complexity: O(n)
module.exports = function isBST(node, min = Number.MIN_VALUE, max = Number.MAX_VALUE) {
    if(node === null) return true;

    if(node.val <= min || node.val >= max) return false;

    return isBST(node.left, min, node.val) && isBST(node.right, node.val, max);
};