const Stack = require('./stack');

describe('Stack Data Structure', () => {
    test('should add elements to the stack', () => {
        const stack = new Stack();

        expect(stack.isEmpty).toBeTruthy();
        expect(() => stack.peek()).toThrow('The stack is empty.');

        stack.push('value 1');

        expect(stack.peek()).toBe('value 1');

        stack.push('value 2');

        expect(stack.peek()).toBe('value 2');

        stack.push('value 3');

        expect(stack.peek()).toBe('value 3');

        stack.push('value 4');

        expect(stack.peek()).toBe('value 4');

        expect(stack.isEmpty).toBeFalsy();
    });

    test('should return the element from the stack', () => {
        const stack = new Stack();
        stack.push('value 1');
        stack.push('value 2');
        stack.push('value 3');
        stack.push('value 4');

        expect(stack.isEmpty).toBeFalsy();

        expect(stack.pop()).toBe('value 4');
        expect(stack.peek()).toBe('value 3');

        expect(stack.pop()).toBe('value 3');
        expect(stack.peek()).toBe('value 2');

        expect(stack.pop()).toBe('value 2');
        expect(stack.peek()).toBe('value 1');

        expect(stack.pop()).toBe('value 1');
        expect(() => stack.peek()).toThrow('The stack is empty.');

        expect(() => stack.pop()).toThrow('The stack is empty.');

        expect(stack.isEmpty).toBeTruthy();
    });

    test('should correctly handle one-element stack', () => {
        const stack = new Stack();
        stack.push('value 1');

        expect(stack.isEmpty).toBeFalsy();
        expect(stack.peek()).toBe('value 1');

        stack.pop();

        expect(stack.isEmpty).toBeTruthy();
        expect(() => stack.peek()).toThrow('The stack is empty.');

        stack.push('value 2');

        expect(stack.isEmpty).toBeFalsy();
        expect(stack.peek()).toBe('value 2');
    });

    test('should return the smallest element in the stack', () => {
        const stack = new Stack();
        stack.push(5);
        stack.push(3);
        stack.push(4);

        expect(stack.getMin()).toBe(3);

        stack.pop();
        stack.pop();

        expect(stack.getMin()).toBe(5);

        stack.push(1);
        stack.push(-10);
        stack.push(8);
        stack.push(0);

        expect(stack.getMin()).toBe(-10);
    });

    test('should return the largest element in the stack', () => {
        const stack = new Stack();
        stack.push(5);
        stack.push(3);
        stack.push(4);

        expect(stack.getMax()).toBe(5);

        stack.push(10);
        stack.push(8);
        stack.push(49);

        expect(stack.getMax()).toBe(49);

        stack.pop();

        expect(stack.getMax()).toBe(10);

        stack.pop();
        stack.pop();

        expect(stack.getMax()).toBe(5);
    });
});