const BST = require('./binary-tree');
const isBST = require('./is-BST');

class Node {
    constructor(val) {
        this.val = val;
        this.left = null;
        this.right = null;
    }
}

describe('BST Check', () => {
    test('should return true if the given tree is BST', () => {
        const tree = new BST();
        tree.insert(10);
        tree.insert(5);
        tree.insert(15);
        tree.insert(2);
        tree.insert(12);

        expect(isBST(tree.root)).toBeTruthy();
    });

    test('should return false if the given tree is not BST', () => {
        const root = new Node(10);
        root.left = new Node(5);
        root.right = new Node(15);
        root.left.left = new Node(2);
        root.left.right = new Node(12);

        expect(isBST(root)).toBeFalsy();
    });
});