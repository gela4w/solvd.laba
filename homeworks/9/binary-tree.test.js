const BST = require('./binary-tree');

describe('Binary Search Tree Data Structure', () => {
    const tree = new BST();
    tree.insert(5);
    tree.insert(3);
    tree.insert(7);
    tree.insert(9);
    tree.insert(4);
    tree.insert(8);
    tree.insert(1);

    test('should find the node by the value', () => {
        expect(tree.find(7).right.val).toBe(9);
        expect(tree.find(3).left.val).toBe(1);
    });

    test('should throw an error if there is no node with the passed value', () => {
        expect(() => tree.find(2)).toThrow('There is no such value in the tree.');
        expect(() => tree.find(15)).toThrow('There is no such value in the tree.');

        const emptyTree = new BST();

        expect(() => emptyTree.find(1)).toThrow('The tree is empty.');
    });

    test('should traverse the tree in the pre-order direction', () => {
        expect(tree.preOrder()).toStrictEqual([5, 3, 1, 4, 7, 9, 8]);
    });

    test('should traverse the tree in the in-order direction', () => {
        expect(tree.inOrder()).toStrictEqual([1, 3, 4, 5, 7, 8, 9]);
    });

    test('should traverse the tree in the post-order direction', () => {
        expect(tree.postOrder()).toStrictEqual([1, 4, 3, 8, 9, 7, 5]);
    });
});