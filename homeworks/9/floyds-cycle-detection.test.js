const cycleDetection = require('./floyds-cycle-detection');
const LinkedList = require('./singly-linked-list');

class Node {
    constructor(val) {
        this.val = val;
        this.next = null;
    }
}

describe('Floyd\'s Cycle Detection Algorithm', () => {
    test('should return true if there is a cycle in the linked list', () => {
        const a = new Node(1);
        const b = new Node(2);
        const c = new Node(3);
        const d = new Node(4);
        const e = new Node(5);
        const f = new Node(6);
        const g = new Node(7);

        a.next = b;
        b.next = c;
        c.next = d;
        d.next = e;
        e.next = f;
        f.next = g;
        g.next = d;

        expect(cycleDetection(a)).toBeTruthy();
    });

    test('should return false if there is no cycle in the linked list', () => {
        const list = new LinkedList();
        list.insert('value 1', 0);
        list.insert('value 2', 1);
        list.insert('value 3', 2);
        list.insert('value 4', 3);

        expect(list.hasCycle()).toBeFalsy();
    });
});