const cycleDetection = require('./floyds-cycle-detection');

/** Class representing a node in the linked list. */
class Node {
    /**
     * Create a new node.
     * @param {*} val - The value to be stored in the node.
     */
    constructor(val) {
        this.val = val;
        this.next = null;
    }
}

/** Class representing a singly linked list data structure. */
class LinkedList {
    /**
     * Create a singly linked list.
     */
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    /**
     * Check if the linked list is empty.
     * @type {boolean}
     * Time complexity: O(1)
     */
    get isEmpty() {
        return this.length === 0;
    }

    /**
     * Add a value to the end of the linked list.
     * @param {*} val - The value to be added.
     * @private
     * Time complexity: O(1)
     */
    #push(val) {
        const node = new Node(val);

        if(this.isEmpty) {
            this.head = node;
            this.tail = node;
        } else {
            this.tail.next = node;
            this.tail = node;
        }

        this.length++;
    }

    /**
     * Remove the last value from the linked list.
     * @return {*} The value removed from the list.
     * @private
     * Time complexity: O(n)
     */
    #pop() {
        let value;

        if(this.length === 1) {
            value = this.head.val;
            this.head = null;
            this.tail = null;
        } else {
            const tail = this.tail;
            let el = this.head.next;

            while (el.next !== tail) {
                el = el.next;
            }

            this.tail = el;
            this.tail.next = null;
            value = tail.val;
        }

        this.length--;

        return value;
    }

    /**
     * Remove the first value from the linked list.
     * @return {*} The value removed from the list.
     * @private
     * Time complexity: O(1)
     */
    #shift() {
        const value = this.head.val;

        if(this.length === 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.head = this.head.next;
        }

        this.length--;

        return value;
    }

    /**
     * Add a value to the beginning of the linked list.
     * @param {*} val - The value to be added.
     * @private
     * Time complexity: O(1)
     */
    #unshift(val) {
        const node = new Node(val);

        if(this.isEmpty) {
            this.head = node;
            this.tail = node;
        } else {
            node.next = this.head;
            this.head = node;
        }

        this.length++;
    }

    /**
     * Get the node by its index.
     * @param {number} ind - The index of the node.
     * @return {Node} The node at the given index.
     * @throws Will throw an error if the index is out of range.
     * @private
     * Time complexity: O(n)
     */
    #getByIndex(ind) {
        if(ind < 0 || ind >= this.length) {
            throw new Error('Incorrect index.');
        }

        let curr = this.head;
        let count = 0;

        while (count !== ind) {
            curr = curr.next;
            count++;
        }

        return curr;
    }

    /**
     * Get the value of the node by its index.
     * @param {number} ind - The index of the node.
     * @return {*} The value of the node at the given index.
     * Time complexity: O(n)
     */
    get(ind) {
        return this.#getByIndex(ind).val;
    }

    /**
     * Insert a value at a specific index in the linked list.
     * @param {*} val - The value to be inserted.
     * @param {number} ind - The index at which to insert the value.
     * @throws Will throw an error if the index is out of range.
     * Time complexity: O(n)
     */
    insert(val, ind) {
        if(ind < 0 || ind > this.length) {
            throw new Error(`Item cannot be inserted. ${ind} is an incorrect index.`);
        }

        if(ind === 0) {
            this.#unshift(val);
        } else if(ind === this.length) {
            this.#push(val);
        } else {
            const node = new Node(val);
            const prev = this.#getByIndex(ind - 1);
            const next = prev.next;

            prev.next = node;
            node.next = next;
            this.length++;
        }
    }

    /**
     * Remove a value at a specific index in the linked list.
     * @param {number} ind - The index of the value to be removed.
     * @return {*} The value removed from the list.
     * @throws Will throw an error if the list is empty or the index is out of range.
     * Time complexity: O(n)
     */
    remove(ind) {
        if(this.isEmpty) {
            throw new Error('The list is empty.');
        }

        if(ind < 0 || ind >= this.length) {
            throw new Error(`Item cannot be removed. ${ind} is an incorrect index.`);
        }

        if(ind === 0) {
            return this.#shift();
        } else if(ind === this.length) {
            return this.#pop();
        } else {
            const prev = this.#getByIndex(ind - 1);
            const removed = prev.next;

            prev.next = removed.next;
            this.length--;

            return removed.val;
        }
    }

    /**
     * Find the index of the node by its value.
     * @param {*} val - The value to find.
     * @return {number} The index of the node with the given value.
     * @throws Will throw an error if the value is not found.
     * Time complexity: O(n)
     */
    findIndexByValue(val) {
        let node = this.head;
        let index = 0;

        while(node && node.val !== val) {
            node = node.next;
            index++;
        }

        if(node === null) {
            throw new Error('There is no such element in the list.');
        }

        return index;
    }

    /**
     * Check if the linked list has a cycle.
     * @return {boolean} True if the list has a cycle, false otherwise.
     * Time complexity: O(n)
     */
    hasCycle() {
        if(!this.isEmpty) {
            return cycleDetection(this.head);
        }
    }
}

module.exports = LinkedList;