class PriorityQueue {
    constructor() {
        this.queue = [];
    }

    enqueue(val, priority) {
        this.queue.push({ val, priority });
        this.queue.sort((a, b) => a.priority - b.priority);
    }

    dequeue() {
        return this.queue.shift().val;
    }

    isEmpty() {
        return this.queue.length === 0;
    }
}

// Time complexity: O((V + E) log(V))
module.exports = (graph, start, end) => {
    const distances = {};
    const previous = {};
    const queue = new PriorityQueue();

    for(const node in graph) {
        distances[node] = Infinity;
        previous[node] = null;
    }

    distances[start] = 0;
    queue.enqueue(start, 0);

    while(!queue.isEmpty()) {
        const curr = queue.dequeue();

        if(curr === end) {
            const path = [];
            let prev = end;

            while(prev) {
                path.unshift(prev);
                prev = previous[prev];
            }

            return { distance: distances[end], path };
        }

        for(const neighbor in graph[curr]) {
            const distance = distances[curr] + graph[curr][neighbor];

            if (distance < distances[neighbor]) {
                distances[neighbor] = distance;
                previous[neighbor] = curr;
                queue.enqueue(neighbor, distance);
            }
        }
    }

    return { distance: Infinity, path: [] };
}
