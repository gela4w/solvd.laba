const Queue = require('./queue');

// Time complexity: O(V + E)
module.exports = (graph, start, end) => {
    const visited = [start];
    const queue = new Queue();
    const previous = {};

    queue.enqueue(start);

    while(!queue.isEmpty) {
        const vertex = queue.dequeue();

        if(vertex === end) {
            const path = [];
            let prev = end;

            while(prev) {
                path.unshift(prev);
                prev = previous[prev];
            }

            return path;
        }

        graph.adjacencyList[vertex].forEach(neighbour => {
            if(!visited.includes(neighbour)) {
                visited.push(neighbour);
                previous[neighbour] = vertex;
                queue.enqueue(neighbour);
            }
        });
    }

    throw new Error('There is no such path in the graph.');
};
