/** Class representing a node in the queue. */
class Node {
    /**
     * Create a new node.
     * @param {*} val - The value to be stored in the node.
     */
    constructor(val) {
        this.val = val;
        this.next = null;
    }
}

/** Class representing a queue data structure. */
class Queue {
    /**
     * Create a queue.
     */
    constructor() {
        this.first = null;
        this.last = null;
    }

    /**
     * Check if the queue is empty.
     * @type {boolean}
     * Time complexity: O(1)
     */
    get isEmpty() {
        return this.first === null;
    }

    /**
     * Add a value to the end of the queue.
     * @param {*} val - The value to be added to the queue.
     * Time complexity: O(1)
     */
    enqueue(val) {
        const node = new Node(val);

        if(this.isEmpty){
            this.first = node;
            this.last = node;
        } else {
            this.last.next = node;
            this.last = node;
        }
    }

    /**
     * Remove and return the value from the front of the queue.
     * @return {*} The value at the front of the queue.
     * @throws {Error} If the queue is empty.
     * Time complexity: O(1)
     */
    dequeue() {
        if(this.isEmpty) {
            throw new Error('The queue is empty.');
        }

        const first = this.first;
        const second = this.first.next;

        if(this.first === this.last) {
            this.last = null;
        }

        first.next = null;
        this.first = second;

        return first.val;
    }

    /**
     * Get the value at the front of the queue without removing it.
     * @return {*} The value at the front of the queue.
     * @throws {Error} If the queue is empty.
     * Time complexity: O(1)
     */
    peek() {
        if(this.isEmpty) {
            throw new Error('The queue is empty.');
        }

        return this.first.val;
    }
}

module.exports = Queue;