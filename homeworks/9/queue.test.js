const Queue = require('./queue');

describe('Queue Data Structure', () => {
    test('should add elements to the queue', () => {
        const queue = new Queue();

        expect(queue.isEmpty).toBeTruthy();
        expect(() => queue.peek()).toThrow('The queue is empty.');

        queue.enqueue('value 1');

        expect(queue.peek()).toBe('value 1');

        queue.enqueue('value 2');

        expect(queue.peek()).toBe('value 1');

        queue.enqueue('value 3');

        expect(queue.peek()).toBe('value 1');

        queue.enqueue('value 4');

        expect(queue.peek()).toBe('value 1');
        expect(queue.isEmpty).toBeFalsy();
    });

    test('should return the element from the queue', () => {
        const queue = new Queue();
        queue.enqueue('value 1');
        queue.enqueue('value 2');
        queue.enqueue('value 3');
        queue.enqueue('value 4');

        expect(queue.isEmpty).toBeFalsy();

        expect(queue.dequeue()).toBe('value 1');
        expect(queue.peek()).toBe('value 2');

        expect(queue.dequeue()).toBe('value 2');
        expect(queue.peek()).toBe('value 3');

        expect(queue.dequeue()).toBe('value 3');
        expect(queue.peek()).toBe('value 4');

        expect(queue.dequeue()).toBe('value 4');
        expect(() => queue.peek()).toThrow('The queue is empty.');

        expect(() => queue.dequeue()).toThrow('The queue is empty.');

        expect(queue.isEmpty).toBeTruthy();
    });

    test('should correctly handle one-element queue', () => {
        const queue = new Queue();
        queue.enqueue('value 1');

        expect(queue.isEmpty).toBeFalsy();
        expect(queue.peek()).toBe('value 1');

        queue.dequeue();

        expect(queue.isEmpty).toBeTruthy();
        expect(() => queue.peek()).toThrow('The queue is empty.');

        queue.enqueue('value 2');

        expect(queue.isEmpty).toBeFalsy();
        expect(queue.peek()).toBe('value 2');
    });
});