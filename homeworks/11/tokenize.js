const { SPECIAL_SYMBOLS, STRUCTURE_SYMBOLS, SPECIAL_VALUES } = require('./constants');

module.exports.tokenize = (str) => {
    let tokens = [];
    let i = 0;

    while(i < str.length) {
        let char = str[i];

        if(STRUCTURE_SYMBOLS.includes(char)) {
            tokens.push(char);
            i++;
        } else if(char === SPECIAL_SYMBOLS.doubleQuote) {
            let start = i;
            i++;

            while(i < str.length && str[i] !== SPECIAL_SYMBOLS.doubleQuote) {
                if(str[i] === SPECIAL_SYMBOLS.escapedSlash &&
                    (str[i + 1] === SPECIAL_SYMBOLS.doubleQuote || str[i + 1] === SPECIAL_SYMBOLS.escapedSlash)) {
                    i++; // Skip escaped character
                }
                i++;
            }

            tokens.push(str.slice(start, i + 1));
            i++;
        } else if(/\d/.test(char) || char === SPECIAL_SYMBOLS.minus) { // Digits match
            let start = i;

            while(i < str.length && /[\d.-]/.test(str[i])) { // 123; 12.34; -123
                i++;
            }

            tokens.push(str.slice(start, i));
        } else if (/\s/.test(char)) { // Space characters check
            i++;
        } else if(SPECIAL_VALUES.some(val => str.startsWith(val, i))) {
            const val = SPECIAL_VALUES.find(val => str.startsWith(val, i));

            tokens.push(val);
            i += val.length;
        } else {
            let msg;

            if(char === SPECIAL_SYMBOLS.singleQuote) {
                msg = 'Single quotes are not allowed.';
            } else if(!char.startsWith(SPECIAL_SYMBOLS.doubleQuote)) {
                msg = 'Unquoted keys are not allowed.';
            } else {
                msg = `Unexpected character: ${char}`;
            }

            throw new SyntaxError(msg);
        }
    }

    return tokens;
};
