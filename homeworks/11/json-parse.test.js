const { myJSONParse } = require('./json-parse');

describe('Custom JSON Parse Function', () => {
    test('should parse string with nested objects and arrays', () => {
        const str = `{
            "array": [1, 2, 3],
            "obj": {
                "prop1": true,
                "prop2": 456.10,
                "prop3": {
                    "prop4": {
                        "prop5": "Object's \\"string\\" value!",
                        "prop6": ["a", "b", { "c": "d", "e": false }],
                        "prop7": null
                    }
                },
                "prop8": "String \\n with \\n \\t tabulation"
            }
        }`;

        expect(JSON.parse(str)).toStrictEqual(myJSONParse(str));
    });

    test('should parse simple strings', () => {
        const emptyObjStr = "{}";
        const booleanStr = "true";
        const str = '"foo"';
        const arrayStr = '[1, 5, "false"]';
        const nullStr = "null";

        expect(JSON.parse(emptyObjStr)).toStrictEqual(myJSONParse(emptyObjStr));
        expect(JSON.parse(booleanStr)).toStrictEqual(myJSONParse(booleanStr));
        expect(JSON.parse(str)).toStrictEqual(myJSONParse(str));
        expect(JSON.parse(arrayStr)).toStrictEqual(myJSONParse(arrayStr));
        expect(JSON.parse(nullStr)).toStrictEqual(myJSONParse(nullStr));
    });

    test('should throw an error if JSON syntax errors occur', () => {
        const unquotedKey = '{unquotedKey: 1}';
        const singleQuotesKey = '{\'a\': 1}';
        const singleQuotesValue = '{"a": \'1\'}';
        const objWithTrailingComa = '{"a": 1, }';
        const arrWithTrailingComa = '{"a": 1, "b": [1, ]}';
        const invalidKeyValueStructure = '{"a" 1, "b" 2}';
        const incompleteObj = '{"a": 1';
        const incompleteArr = '[1, 2';
        const incompleteStr = '{"name": "John';
        const strWithExtraSymbols = '{"name": "John"} "something here"';
        const emptyStr = '';

        expect(() => myJSONParse(unquotedKey)).toThrow('Unquoted keys are not allowed.');
        expect(() => myJSONParse(singleQuotesKey)).toThrow('Single quotes are not allowed.');
        expect(() => myJSONParse(singleQuotesValue)).toThrow('Single quotes are not allowed.');
        expect(() => myJSONParse(objWithTrailingComa)).toThrow('No trailing commas are allowed.');
        expect(() => myJSONParse(arrWithTrailingComa)).toThrow('No trailing commas are allowed.');
        expect(() => myJSONParse(invalidKeyValueStructure)).toThrow('Expected colon after key in object.');
        expect(() => myJSONParse(incompleteObj)).toThrow('Unexpected end of JSON input.');
        expect(() => myJSONParse(incompleteArr)).toThrow('Unexpected end of JSON input.');
        expect(() => myJSONParse(incompleteStr)).toThrow('Unexpected end of JSON input.');
        expect(() => myJSONParse(strWithExtraSymbols)).toThrow('Unexpected token at the end of JSON input.');
        expect(() => myJSONParse(emptyStr)).toThrow('An empty string was provided.');
    });
});