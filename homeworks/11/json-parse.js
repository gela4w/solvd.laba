const { tokenize } = require('./tokenize');
const { SPECIAL_SYMBOLS} = require('./constants');

function parseValue(tokens) {
    const token = tokens.shift();

    if(token === undefined) {
        throw new SyntaxError('Unexpected end of JSON input.');
    }

    if(token === SPECIAL_SYMBOLS.openCurly) {
        return parseObject(tokens);
    } else if(token === SPECIAL_SYMBOLS.openSquare) {
        return parseArray(tokens);
    } else if(token.startsWith(SPECIAL_SYMBOLS.doubleQuote)) {
        return parseString(token.slice(1, -1)); // Remove surrounding quotes
    } else if(token === 'true') {
        return true;
    } else if(token === 'false') {
        return false;
    } else if(token === 'null') {
        return null;
    } else if(!isNaN(token)) {
        return parseFloat(token);
    } else {
        const msg = token === SPECIAL_SYMBOLS.closeSquare || SPECIAL_SYMBOLS.closeCurly ?
            'No trailing commas are allowed.' : `Unexpected character: ${token}.`;

        throw new SyntaxError(msg);
    }
}

function parseObject(tokens) {
    const obj = {};

    while(tokens[0] !== SPECIAL_SYMBOLS.closeCurly) {
        if(tokens[0] === SPECIAL_SYMBOLS.comma) {
            tokens.shift(); // Remove comma
        }

        const key = parseValue(tokens);

        if(tokens.shift() !== SPECIAL_SYMBOLS.semicolon) {
            throw new SyntaxError('Expected colon after key in object.');
        }

        obj[key] = parseValue(tokens);
    }

    tokens.shift(); // Remove closing brace

    return obj;
}

function parseArray(tokens) {
    const arr = [];

    while(tokens[0] !== SPECIAL_SYMBOLS.closeSquare) {
        if(tokens[0] === SPECIAL_SYMBOLS.comma) {
            tokens.shift(); // Remove comma
        }

        const value = parseValue(tokens);
        arr.push(value);
    }

    tokens.shift(); // Remove closing bracket

    return arr;
}

function parseString(token) {
    return token.replace(/\\["\\\/nt]/g, match => {
        // Range: " or / or \ or n or t
        switch(match[1]) {
            case '"': return '"';
            case '\\': return '\\';
            case '/': return '/';
            case 'n': return '\n';
            case 't': return '\t';
            default: throw new SyntaxError(`Invalid escape sequence: \\${match[1]}.`);
        }
    });
}

module.exports.myJSONParse = (str) => {
    const tokens = tokenize(str);

    if(tokens.length === 0) {
        throw new SyntaxError('An empty string was provided.');
    }

    const result = parseValue(tokens);

    if(tokens.length !== 0) {
        throw new SyntaxError('Unexpected token at the end of JSON input.');
    }

    return result;
};
