const SPECIAL_SYMBOLS = {
    openCurly: '{',
    closeCurly: '}',
    openSquare: '[',
    closeSquare: ']',
    semicolon: ':',
    doubleQuote: '"',
    singleQuote: '\'',
    comma: ',',
    escapedSlash: '\\',
    minus: '-',
};

const STRUCTURE_SYMBOLS = [
    SPECIAL_SYMBOLS.openCurly,
    SPECIAL_SYMBOLS.closeCurly,
    SPECIAL_SYMBOLS.openSquare,
    SPECIAL_SYMBOLS.closeSquare,
    SPECIAL_SYMBOLS.semicolon,
    SPECIAL_SYMBOLS.comma,
];

const SPECIAL_VALUES = ['true', 'false', 'null'];

module.exports = { SPECIAL_SYMBOLS, STRUCTURE_SYMBOLS, SPECIAL_VALUES };