const { tokenize } = require('./tokenize');
describe('Tokenize', () => {
    test('should split the string into the valid JSON tokens', () => {
        const string = `{"name": "Pike\'s Peak", "temp": -30.25, "iron": null, 
        "gems": "Pink granite: \\"Pikes Peak granite\\".", "isNational": true}`;

        const result = ['{', '"name"', ':', '"Pike\'s Peak"', ',', '"temp"', ':', '-30.25', ',', '"iron"',
            ':', 'null', ',', '"gems"', ':', '"Pink granite: \\"Pikes Peak granite\\"."', ',', '"isNational"',
            ':', 'true', '}'];

        expect(tokenize(string)).toStrictEqual(result);
    });

    test('should throw an error if string contains an invalid JSON token', () => {
        const str1 = '{"validKey": 1, unquotedKey: 2}';

        expect(() => tokenize(str1)).toThrow('Unquoted keys are not allowed.');
    });
});