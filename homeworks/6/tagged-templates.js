function _buildTemplateString(strings, values) {
    let str = '';
    for(let i = 0; i < strings.length; i++) {
        str += strings[i] + (values[i] || '');
    }

    return str;
}

// Task 1: Quasi-Tagged Templates
module.exports.localize = (translations, lang = 'en', ) => {
    return function(strings, ...values) {
        const translation = translations[lang];
        const translatedValues = values.map(val => translation[val]);

        return _buildTemplateString(strings, translatedValues);
    }
};

// Task 2: Advanced Tagged Template
module.exports.highlightKeywords = (template, keywords) => {
    let result = template;
    for(let i = 0; i < keywords.length; i++) {
        let replace = '${' + i + '}';
        let replacement = `<span class='highlight'>${keywords[i]}</span>`;
        result = result.replaceAll(replace, replacement);
    }
    return result;
};

// Task 3: Multiline Tagged Template
module.exports.multiline = (strings) => {
    return strings[0].split('\n').map((val, ind) => `${ind + 1} ${val}`).join('\n');
};

// Task 4: Implementing Debounce Function
module.exports.debounce = (fn, delay) => {
    let timer;

    return function(...args) {
        clearTimeout(timer);
        timer = setTimeout(() => fn.apply(this, args), delay);
    }
};

// Task 5: Implementing Throttle Function
module.exports.throttle = (fn, interval) => {
    let isRunning = false;

    return function(...args) {
        if (!isRunning) {
            isRunning = true;
            fn.apply(this, args);
            setTimeout(() => isRunning = false, interval);
        }
    }
};

// Task 6: Currying Function Implementation
module.exports.curry = (func, arity) => {
    let _count = arity;
    let _args = [];

    return function curried(arg) {
        _count--;
        _args.push(arg);
        if (_count === 0) {
            return func.apply(this, _args);
        } else {
            return function(arg2) {
                return curried.call(this, arg2);
            }
        }
    };
};