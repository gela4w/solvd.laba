const { localize, highlightKeywords, multiline, debounce, curry } = require('./tagged-templates');
jest.useFakeTimers();

describe('Tagged Templates, Debounce, Throttling, Currying', () => {
    describe('Localize', () => {
        const translations = {
            en: {
                greet: "Hello",
                intro: "Welcome to our website"
            },
            fr: {
                greet: "Bonjour",
                intro: "Bienvenue sur notre site web"
            }
        };

        const language = "fr";
        const greeting = "greet";
        const introduction = "intro";

        test('should translate the string', () => {
            const localizedGreeting = localize(translations, language)`${greeting} Angelina`;
            const localizedIntroduction = localize(translations)`${introduction}`;
            const localizedString = localize(translations, language)`${greeting} Angelina. ${introduction}`;

            expect(localizedGreeting).toBe(`${translations.fr.greet} Angelina`);
            expect(localizedIntroduction).toBe(translations.en.intro);
            expect(localizedString).toBe(`${translations.fr.greet} Angelina. ${translations.fr.intro}`);
        });
    });

    describe('HighlightKeywords', () => {
        test('should replace words in a template', () => {
            const keywords = ["JavaScript", "template", "tagged"];
            const template = "Learn \${0} tagged templates to create custom \${1} literals for \${2} manipulation. This is \${0}, baby!";
            const highlighted = highlightKeywords(template, keywords);
            const result = "Learn " +
                "<span class='highlight'>JavaScript</span> " +
                "tagged templates to create custom " +
                "<span class='highlight'>template</span> " +
                "literals for " +
                "<span class='highlight'>tagged</span> " +
                "manipulation. " +
                "This is " +
                "<span class='highlight'>JavaScript</span>" +
                ", baby!";


            expect(highlighted).toBe(result);
        });
    });

    describe('Multiline', () => {
        test('should add line number to the passed code snippet', () => {
            const test = multiline`
        function add(a, b) {
            return a + b;
        }`;
            const res = `1 
2         function add(a, b) {
3             return a + b;
4         }`;

            expect(test).toBe(res);
        });
    });

    describe('Debounce', () => {
        test('should debounce function execution for specified time', () => {
            const fn = jest.fn();
            debounce(fn, 3000)();

            expect(fn).not.toHaveBeenCalled();

            jest.runAllTimers();

            expect(fn).toHaveBeenCalled();
        });
    });

    describe('Currying', () => {
        test('should make passed function curried', () => {
            function multiply(a, b, c) { return a * b * c; }

            const curriedMultiply = curry(multiply, 3);

            const step1 = curriedMultiply(2);
            const step2 = step1(3);
            const result = step2(4);

            expect(result).toBe(24);
        });
    });
});
