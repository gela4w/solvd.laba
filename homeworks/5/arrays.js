module.exports.customFilterUnique = (arr, cb) => {
    const map = {};

    arr.forEach(el => {
        const key = cb(el);

        if(Object.hasOwn(map, key)) {
            map[key] = null;
        } else {
            map[key] = el;
        }
    });

    return Object.values(map).filter(val => val !== null);
};

module.exports.chunkArray = (arr, size) => {
    let index = 0;
    let resIndex = 0;
    const result = new Array(Math.ceil(arr.length / size));

    while(index < arr.length) {
        result[resIndex++] = arr.slice(index, index += size);
    }
    return result;
};

module.exports.customShuffle = (arr) => {
    const shuffled = [...arr];

    for(let i = arr.length - 1; i >= 1; i--) {
        let j = Math.floor(Math.random() * i);
        [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
    }
    return shuffled;
};

module.exports.getArrayIntersection = (arr1, arr2) => {
    const set1 = new Set(arr1);
    const set2 = new Set(arr2);
    const res = [];

    for(let key of set1) {
        if(set2.has(key)) {
            res.push(key);
        }
    }

    return res;
};

module.exports.getArrayUnion = (arr1, arr2) => {
    return Array.from(new Set([ ...arr1, ...arr2]));
};

module.exports.measureArrayPerformance = (fn, arr) => {
    const startTime = performance.now();
    fn(arr);
    const endTime = performance.now();

    return endTime - startTime;
};