const { customFilterUnique, chunkArray, customShuffle, getArrayIntersection, getArrayUnion} = require('./arrays');

describe('Arrays', () => {
    describe('Advanced Array Filtering', () => {
        test('should filter array based on passed condition', () => {
            const arr1 = [
                { id: 1, name: 'test' },
                { id: 2, name: 'foo' },
                { id: 3, boo: 3 },
                { id: 2, isAdmin: false },
            ];

            expect(customFilterUnique(arr1, el => el.id))
                .toStrictEqual([ {id: 1, name: 'test'}, {id: 3, boo: 3} ]);

            const arr2 = [1, 2, 'a', 'b', 2, 'b', 3];

            expect(customFilterUnique(arr2, el => el)).toStrictEqual([1, 3, 'a']);
        });
    });

    describe('Array Chunking', () => {
        test('should split array on the specified amount of chunks', () => {
            const arr = [1, 2, 3, ['a', 12, 'b'], 4, 5, 6, { c: 13 }, 7, 8, 9, 10, { d: 14, e: 15}];
            const splitArray = [
                [1, 2, 3],
                [['a', 12, 'b'], 4, 5],
                [6, { c: 13 }, 7],
                [8, 9, 10],
                [{ d: 14, e: 15}]
            ];

            expect(chunkArray(arr, 3)).toStrictEqual(splitArray);
        });
    });

    describe('Array Shuffling', () => {
        test('should shuffle array', () => {
            const arr = [1, 2, 3, [4, 5, 6, 7], 8, 9, 'a', 'b', 'c', 'd', { op1: 'op1' }, { op2: 'op2', op3: 'op3' }];

            expect(customShuffle(arr)).not.toStrictEqual(arr);
            expect(customShuffle(arr)).not.toStrictEqual(arr);
            expect(customShuffle(arr)).not.toStrictEqual(arr);
        });
    });

    describe('Array Intersection and Union', () => {
        const arr1 = [1, 2, 3, 4, 5, 2, 'a'];
        const arr2 = ['a', 'b', 'c', 2, 'd', 3];

        test('should return arrays\' intersection', () => {
            expect(getArrayIntersection(arr1, arr2)).toStrictEqual([2, 3, 'a']);
        });

        test('should return arrays\' unique values', () => {
            expect(getArrayUnion(arr1, arr2)).toStrictEqual([1, 2, 3, 4, 5, 'a', 'b', 'c', 'd']);
        });
    });
});